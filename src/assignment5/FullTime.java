package assignment5;

/**
 * This is the FullTime class which extends Faculty
 * Created by Jorge Palacios on 10/2/2018.
 * @author Jorge Palacios
 */
public class FullTime extends Faculty {

    private String rank;

    /**
     * This constructor for the FullTime class
     * ensures the rank property will default to
     * "N/A" as a String values if a FullTime
     * object was created but no values were set.
     */
    public FullTime(){
        this.rank = "N/A";
    }

    /**
     * This is the getter used to access the value
     * of rank. When called
     * it simply returns whatever value is assigned to rank.
     * @return rank
     */
    public String getRank()
    {
        return this.rank;
    }

    /**
     * This is the setter used to take the user's input and assign a String value
     * to the rank property.
     * This setter takes the user input, compares the input to acceptable cases in a switch statement,
     * then validates it for acceptable responses.
     * If the user input is not accepted then the setter returns false.
     * @param rank - Holds the user input to be used in the switch statement in setRank.
     * @return true if user input passes validation.
     */
    public boolean setRank(String rank)
    {
        switch (rank)
        {
            case "1":
                this.rank = "Lecturer";
                return true;
            case "2":
                this.rank = "Assistant Professor";
                return true;
            case "3":
                this.rank = "Associate Professor";
                return true;
            case "4":
                this.rank = "Full Professor";
                return true;
            default:
                System.out.println("Please enter your correct rank" +
                        "\n1 for Lecturer" +
                        "\n2 for Assistant Professor" +
                        "\n3 for Associate Professor" +
                        "\n4 for Full Professor");
                return false;
        }
    }

    /**
     *This toString method overrides the super toString method and
     * concatenates the values of rank
     * to the information already stored in the object.
     * @return rank
     */
    public String toString() {

        String fullTimeData =   "\n*Full-time*" +
                "\nRank: "+getRank();

        return super.toString() + fullTimeData;
    }
}
