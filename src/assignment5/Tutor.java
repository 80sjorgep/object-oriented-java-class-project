package assignment5;


import java.util.regex.Pattern;

/**
 * This is the Tutor class which extends Student
 * Created by Jorge Palacios on 10/2/2018.
 * @author Jorge Palacios
 */
public class Tutor extends Student implements Employee {

    final Pattern charPattern = Pattern.compile("^[a-zA-Z, ]+$");

    private String topics;
    private double hours;
    private String startDate;
    private double salary;

    /**
     * This constructor for the Tutor class
     * ensures the topics, hours properties will default to
     * "N/A" for all String values and 0 for all numeric values if a Tutor
     * object was created but no values were set.
     */
    public Tutor()
    {
        this.topics = "N/A";
        this.hours = 0.0;
        this.startDate = "N/A";
        this.salary = 0.00;
    }

    /**
     * This is the getter used to access the value
     * of topics set by a tutor. When called
     * it simply returns whatever value is assigned to topics.
     * @return topics
     */
    public String getTopics() {
        return topics;
    }

    /**
     * This is the setter used to take the user's input and assign a String value
     * to topics.
     * This setter takes the user input, compares the input to an acceptable pattern and character count,
     * then validates the input.
     * If the user input is not accepted then the setter returns false.
     * @param topics - holds the user input as a string to be checked for validation in setTopics.
     * @return true if user input passes validation.
     */
    public boolean setTopics(String topics) {

        if (!charPattern.matcher(topics).matches())
        {
            System.out.println("Only Upper-case and lower-case letters, commas, and spaces accepted");
            return false;
        }

        this.topics = topics;
        return true;
    }

    /**
     * This is the getter used to access the value
     * of hours set by a student. When called
     * it simply returns whatever value is assigned to hours.
     * @return classStanding
     */
    public double getHours() {
        return hours;
    }

    /**
     * This is the setter used to take the user's input and assign a double value
     * to hours.
     * This setter takes the user input, compares the input to an
     * acceptable value between 0 and 1000, then validates it.
     * If the user input is not accepted then the setter returns false.
     * @param hours - holds the user input as a parsed string to double to be checked for validation in setHours.
     * @return true if user input passes validation.
     */
    public boolean setHours(double hours) {

        if (hours < 0 || hours > 1000)
        {
            System.out.println("Number of hours tutored must be between 0 and 1000");
            return false;
        }

        this.hours = hours;
        return true;
    }

    /**
     *This toString method overrides the super toString method and
     * concatenates the values of topics and hours
     * to the information already stored in the object.
     */
    public String toString()
    {
        String tutorInfo = 	"\n**Tutor**" +
                "\nTopics: " + getTopics() +
                "\nHours: " + getHours() +
                "\nStart-Date: " + getStartDate() +
                "\nSalary: $" + getSalary();

        return super.toString() + tutorInfo;
    }

    @Override
    public boolean setSalary(double salary) {
        if(salary < 0)
        {
            System.out.println("Cannot enter a salary less than 0!");
            return false;
        }
        this.salary = salary;
        return true;
    }

    @Override
    public double getSalary() {
        return this.salary;
    }

    @Override
    public boolean setStartDate(String startDate) {
        final Pattern startDatePattern =
                Pattern.compile("((?:19|20)\\d\\d)/(0?[1-9]|1[012])/([12][0-9]|3[01]|0?[1-9])");

        if (startDate.length() != 10 || !startDatePattern.matcher(startDate).matches())
        {
            System.out.println("Please enter your start-date as YYYY/MM/DD with slashes");
            return false;
        }
        if (startDate == null)
        {
            System.out.println("Please enter your start-date as YYYY/MM/DD with slashes");
            return false;
        }

        this.startDate = startDate;
        return true;
    }

    @Override
    public String getStartDate() {
        return this.startDate;
    }
}
