package assignment5;


import java.util.regex.Pattern;

/**
 * This is the staff class which extends the Person class
 * Created by Jorge Palacios on 10/2/2018.
 * @author Jorge Palacios
 */
public class Staff extends Person implements  Employee{
    /**
     * This name pattern, which is a regular expression, is used in the setDepartment setSupervisor method to ensure only acceptable values are received
     */
    final Pattern NAME_PATTERN = Pattern.compile("^[A-Za-z-' ]+$");

    private String department;
    private String supervisor;
    //private String startDate;
    private double salary;

    /**
     * This constructor for the staff class
     * ensures department and supervisor will default to
     * "N/A" if a Staff object is created but no values were set.
     */
    public Staff ()
    {
        this.department = "N/A";
        this.supervisor = "N/A";
        this.startDate = "N/A";
        this.salary = 0.00;
    }

    /**
     * This is the getter used to access the value
     * of department set by a staff member. When called
     * it simply returns whatever value is assigned to department.
     * @return department.
     */
    public String getDepartment() {
        return department;
    }

    /**
     * This is the setter used to take the user's input and assign a String value
     * to department by a staff member.
     * This setter takes the user input, compares the input to the name pattern
     * and acceptable length of letters,
     * then validates it for acceptable responses.
     * If the user input is not accepted then the setter returns false.
     * @param department - holds the user input as a string to be checked for validation in setDepartment.
     * @return true if user input passes validation.
     */
    public boolean setDepartment(String department) {

        if (department.length() < 3 || department.length() > 50){

            System.out.println("Enter between 3 and 50 characters only!");
            return  false;
        }

        if (!NAME_PATTERN.matcher(department).matches()) {

            System.out.println("Only enter Upper-case and lower-case letters, dashes, apostrophes, and spaces");
            return false;
        }

        this.department = department;
        return true;
    }

    /**
     * This is the getter used to access the value
     * of supervisor set by a staff member. When called
     * it simply returns whatever value is assigned to supervisor.
     * @return department.
     */
    public String getSupervisor() {
        return supervisor;
    }

    /**
     * This is the setter used to take the user's input and assign a String value
     * to staff member's supervisor.
     * This setter takes the user input, compares the input to the name pattern
     * and acceptable length of letters,
     * then validates it for acceptable responses.
     * If the user input is not accepted then the setter returns false.
     * @param supervisor - holds the user input as a string to be checked for validation in setSupervisor.
     * @return true if user input passes validation.
     */
    public boolean setSupervisor(String supervisor) {

        if(supervisor.length() < 2 || supervisor.length() > 50)
        {
            System.out.println(	"Supervisor name cannot be " + supervisor.length() + " characters" +
                    " \nMin 2 letters, max 50 letters");
            return false;
        }

        if (!NAME_PATTERN.matcher(supervisor).matches()) {

            System.out.println("Only enter Upper-case and lower-case letters, dashes, apostrophes, and spaces");

            return false;
        }

        this.supervisor = supervisor;
        return true;
    }

    /**
     *This toString method overrides the super toString method and
     * concatenates the values for supervisor and department to the other information
     * already stored in object.
     * @return coursesTaught.
     */
    public String toString()
    {
        String staffInfo = 	"\n**Staff**" +
                "\nDepartment: " + getDepartment() +
                "\nSupervisor: " + getSupervisor() +
                "\nStart-Date: " + getStartDate() +
                "\nSalary: $" + getSalary();

        return super.toString() + staffInfo;
    }

    @Override
    public boolean setSalary(double salary) {
        if(salary < 0)
        {
            System.out.println("Cannot enter a salary less than 0!");
            return false;
        }
        this.salary = salary;
        return true;
    }

    @Override
    public double getSalary() {
        return this.salary;
    }

    @Override
    public boolean setStartDate(String startDate) {

        this.startDate = startDate;

        final Pattern startDatePattern =
                Pattern.compile("((?:19|20)\\d\\d)/(0?[1-9]|1[012])/([12][0-9]|3[01]|0?[1-9])");

        if (startDate.length() != 10 || !startDatePattern.matcher(startDate).matches())
        {
            System.out.println("Please enter your start-date as YYYY/MM/DD with slashes");
            return false;
        }

        boolean isOver1 = setYearsOfEmployment();

        if(isOver1)
        {
            this.startDate = startDate;
            return true;
        }
        else
        {
            System.out.println("Please enter a correct start-date that is older than today's date!");
            return false;
        }

    }
}
