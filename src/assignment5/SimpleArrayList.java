package assignment5;


/**
 * Created by Jorge Palacios on 11/26/2018.
 */
public class SimpleArrayList<T>
{
    //the constructor method for the SimpleArrayList generic
    public SimpleArrayList()
    {
        startingCapacity = 10;
        size = 0;
        data = new Object[startingCapacity];
    }

    protected Object[] data;
    protected int startingCapacity;
    protected int size;

    /**
     * adds an element at the end of array
     */
    public void add(T object)
    {
        if(size >= data.length)
        {
            Object[] tempArray = new Object[size + startingCapacity];

            for(int i = 0; i < size; i++)
            {
                tempArray[i] = object;
            }

            data = tempArray;
        }
        data[size++] = object;
    }

    /*
     * adds an element at a specific element
     */
    public boolean add(T object, int index)
    {
        //the element equal to size is empty, we can only add to an element id that is occupied in order to
        //keep the array from having elements occupied that are not in order
        if(index > size-1)
        {
            //technically it should read - Choose an element within between 0 and " + (size-1)
            //but was modified to match the needs of someone using the Assignment 5 app
            System.out.println("Choose an element within between 1 and " + (size+1) + " Person NOT added!!!");
            return false;
        }

        if(index <= size)
        {
            System.out.println("COPYING PROCESS STARTED!!!");
            Object[] tempArray;

            size++;

            if(size >= data.length)
            {
                tempArray = new Object[size + startingCapacity];

                System.out.println("size >= data.length - temp array length = " + tempArray.length);

                //accessing the
                tempArray[index] = object;

                //we intentionally do not access the selected index as it was handled earlier
                //we copy over all the data before the selected index but not at the selected index
                for (int i = 0; i < index; i++)
                {
                    tempArray[i] = data[i];
                    System.out.println(tempArray[i].toString());
                }

                //we intentionally skipp the selected index as it was handled earlier
                //we copy over all the data after the selected index but not at the selected index
                try
                {
                    for(int i = index; i < data.length; i++)
                    {
                        int tempIndexAccessor = i + 1;

                        tempArray[tempIndexAccessor] = data[i];
                    }
                } catch (Exception e) {
                    System.out.println("Error: "+e.getMessage());
                    //e.printStackTrace();
                }

                data = tempArray;
                return true;
            }

            if(size < data.length)
            {
                tempArray = new Object[size];

                //System.out.println("size < data.length - temp array length = " + tempArray.length);

                //accessing the
                tempArray[index] = object;

                //we intentionally do not access the selected index as it was handled earlier
                //we copy over all the data before the selected index but not at the selected index
                for (int i = 0; i < index; i++)
                {
                    tempArray[i] = data[i];
                }


                //we intentionally skipp the selected index as it was handled earlier
                //we copy over all the data after the selected index but not at the selected index

                try
                {
                    for(int i = index+1; i < size; i++)
                    {
                        int tempIndexAccessor = i - 1;
                        System.out.println("iterator = "+i);
                        tempArray[i] =
                                data[tempIndexAccessor];

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                data = tempArray;
                return true;
            }
        }
        return false;
    }

    public Object get(int i)
    {
        if(i < 0 || i >= size)
        {
            //invalid index
            System.out.println("Please select a correct number");
            return Integer.MIN_VALUE;
        }
        return  data[i];
    }

    /**
     * This remove method removes an element according to it's position in the
     * array not the value stored in that position
     * @param elementPosition
     */
    public void remove(int elementPosition)
    {
        Object[] tempArray;

        //we reduce the entered elemPos by one to account for the array being zero-based
        //and the array elements being displayed as 1,2,3... not 0,1,2,3...
        int elemPosToBeRemoved = elementPosition - 1;

        if(elemPosToBeRemoved > size - 1 || elemPosToBeRemoved < 0)
        {
            System.out.println("please select the correct number in the list before attempting to remove");
            return;
        }
        if(size <= 0)
        {
            System.out.println("please add to the list before attempting to remove");
            return;
        }

        if(size > 0)
        {
            //we are setting the capacity of the temp array lower by 1 because we are removing one element
            tempArray = new Object[data.length];

            // we do not access the selected index so it will be skipped in the copying process
            // we copy over all the data before the selected index but not at the selected index
            for (int i = 0; i < elemPosToBeRemoved; i++)
            {
                tempArray[i] = data[i];
            }

            //now we copy the data array into the temp array skipping the 'deleted' element
            for(int i = elemPosToBeRemoved + 1; i < data.length; i++)
            {
                int tempIndexAccessor = i - 1;

                tempArray[tempIndexAccessor] = data[i];
            }

            data = tempArray;
            size--;

            return;
        }
    }

    /**
     * This method removes a value from the array by searching for the value entered
     * then removing it from the array
     * @param elementObject
     */
    public void removeByValue(Object elementObject)
    {
        int elementID = 0;
        Object[] tempArray;

        if(size <= 0)
        {
            System.out.println("please add to the list before attempting to remove");
            return;
        }

        if(size > 0)
        {
            //we are setting the capacity of the temp array lower by 1 because we are removing one element
            tempArray = new Object[data.length];

            for (int i = 0; i < data.length; i++)
            {
                if (data[i] == elementObject)
                {
                    elementID = i;
                }
            }
            // after getting the element location of the data to be removed
            // we do not access the selected index so it will be skipped in the copying process
            // we copy over all the data before the selected index but not at the selected index
            for (int i = 0; i < elementID; i++)
            {
                tempArray[i] = data[i];
            }

            //now we copy the data array into the temp array skipping the 'deleted' element
            for(int i = elementID + 1; i < data.length; i++)
            {
                int tempIndexAccessor = i - 1;

                tempArray[tempIndexAccessor] = data[i];
            }

            data = tempArray;
            size--;

            return;
        }
    }

    //returns the size of an array
    public int size()
    {
        System.out.println("Size = "+size);
        return size;
    }
}
