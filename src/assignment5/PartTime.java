package assignment5;

/**
 * This is the PartTime class which extends the Faculty class
 * Created by Jorge Palacios on 10/2/2018.
 * @author Jorge Palacios
 */
public class PartTime extends Faculty {

    int coursesTaught;

    /**
     * This constructor for the faculty PartTime class
     * ensures coursesTaught will at least be set
     * to 0 if the class was used to create an object but the value
     * was not set for coursesTaught
     */
    public PartTime()
    {
        this.coursesTaught = 0;
    }

    /**
     * This is the getter used to access the value
     * of coursesTaught by a part time faculty member. When called
     * it simply returns whatever value is assigned to coursesTaught.
     * @return coursesTaught.
     */
    public int getCoursesTaught() {
        return this.coursesTaught;
    }

    /**
     * This is the setter used to take the user's input and assign a value
     * to coursesTaught by a part time faculty member.
     * This setter takes the user input, which is a string parsed into an int value,
     * and validates it for acceptable responses.
     * If the user input is not accepted then the setter returns false.
     * @param coursesTaught - A string holder used to check validation in setCoursesTaught.
     * @return true if user input passes validation.
     */
    public boolean setCoursesTaught(String coursesTaught) {

        int courseIntConverted = Integer.parseInt(coursesTaught);
        String courseTaughtPattern = "\\d+";

        if (coursesTaught.matches(courseTaughtPattern) == false)
        {
            System.out.println("ONLY ENTER NUMBERS! (between 1 and 100)");
            return false;
        }

        if(courseIntConverted < 1 || courseIntConverted > 100)
        {
            System.out.println("Please enter your number of courses taught(between 1 and 100)");
            return false;
        }

        this.coursesTaught = courseIntConverted;
        return true;
    }

    /**
     *This toString method overrides the super toString method and
     * concatenates the number of courses taught to the other information
     * already stored in object.
     * @return coursesTaught.
     */
    public String toString(){

        String partTimeData =   "\n*Part-time*" +
                "\nNumber of courses taught: " + getCoursesTaught();

        return super.toString() + partTimeData;
    }
}

