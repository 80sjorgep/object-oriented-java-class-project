package assignment5;

import java.time.LocalDate;
import java.util.regex.Pattern;

/**
 * This is the Faculty class which extends the Person class
 * Created by Jorge Palacios 10/2/2018.
 * @author Jorge Palacios
 */
public class Faculty extends Person implements Employee{

    final Pattern DIVISION_PATTERN = Pattern.compile("^[A-Za-z-' ]+$");

    private String division;
    private String degree;
    private int yearOfGraduation;
    private String startDate;
    private double salary;

    /**
     * This constructor for the Faculty class
     * ensures the division, degree, and yearOfGraduation properties
     * will default to "N/A" as a String values if a Faculty
     * object was created but no values were set.
     */
    public Faculty(){
        this.division = "N/A";
        this.degree = "N/A";
        this.yearOfGraduation = 0;
        this.startDate = "N/A";
        this.salary = 0.00;
    }

    /**
     * This is the getter used to access the value
     * of division. When called
     * it simply returns whatever value is assigned to division.
     * @return division
     */
    public String getDivision() {return division;}

    /**
     * This is the setter used to take the user's input and assign a String value
     * to the division property.
     * This setter takes the user input, compares the input to an acceptable acceptable pattern
     * and an input length between 2 and 50, then validates it for acceptable responses.
     * If the user input is not accepted then the setter returns false.
     * @param division - Holds the input for user's division so setDivision can check for validity.
     * @return true if user input passes validation.
     */
    public boolean setDivision(String division) {

        if(division.length() < 2 || division.length() > 50)
        {
            System.out.println("Division length = "+ division.length()+" \nMin 2 letters, max 50 letters");
            return false;
        }
        if (!DIVISION_PATTERN.matcher(division).matches()) {

            System.out.println("Only enter Upper-case and lower-case letters, dashes, apostrophes, and spaces");

            return false;
        }
        this.division = division;

        return true;
    }

    /**
     * This is the getter used to access the value
     * of degree. When called
     * it simply returns whatever value is assigned to degree.
     * @return degree
     */
    public String getDegree(){return degree;}

    /**
     * This is the setter used to take the user's input and assign a String value
     * to the degree property.
     * This setter takes the user input, compares the input to acceptable cases in a switch statement,
     * then validates it for acceptable responses.
     * If the user input is not accepted then the setter returns false.
     * @param degree - holds the user input so it can be used for the switch statement in setDegree
     * @return true if user input passes validation.
     */
    public boolean setDegree(String degree) {

        switch (degree.toLowerCase()) {
            case "b":
                this.degree = "Bachelor's";
                return true;
            case "m":
                this.degree = "Master's";
                return true;
            case "d":
                this.degree = "Doctorate";
                return true;
            default:
                System.out.println("Please Enter" +
                        "\nB for Bachelor's" +
                        "\nM for Master's" +
                        "\nD for Doctorate");
                return false;
        }
    }

    /**
     * This is the getter used to access the value
     * of getYearOfGraduation. When called
     * it simply returns whatever value is assigned to getYearOfGraduation.
     * @return getYearOfGraduation
     */
    public int getYearOfGraduation(){return yearOfGraduation;}

    /**
     * This is the setter used to take the user's input and assign an int value
     * to the yearOfGraduation property.
     * This setter takes the user input, compares the input to acceptable range of years,
     * then validates.
     * If the user input is not accepted then the setter returns false.
     * @param yearOfGraduation - holds the yearOfGraduation input by the user to check for validation
     * @return true if user input passes validation.
     */
    public boolean setYearOfGraduation(int yearOfGraduation){

        LocalDate currentYear = LocalDate.now();

        if (yearOfGraduation < 1950 || yearOfGraduation > currentYear.getYear())
        {
            System.out.println("Must enter a year between 1950 and " + currentYear.getYear());
            return false;
        }

        this.yearOfGraduation = yearOfGraduation;
        return true;
    }

    /**
     *This toString method overrides the super toString method and
     * concatenates the values of division, degree, and yearOfGraduation
     * to the information already stored in the object.
     * @return division
     * @return degree
     * @return yearOfGraduation
     */
    public String toString(){

        String facultyData =    "\n*Faculty*" +
                "\nDivision: " + getDivision() +
                "\nDegree: " + getDegree() +
                "\nYear of Graduation : " + getYearOfGraduation() +
                "\nStart-Date: " + getStartDate() +
                "\nSalary: $" + getSalary();

        return super.toString() + facultyData;
    }

    @Override
    public boolean setSalary(double salary) {
        if(salary < 0)
        {
            System.out.println("Cannot enter a salary less than 0!");
            return false;
        }
        this.salary = salary;
        return true;
    }

    @Override
    public double getSalary() {
        return this.salary;
    }

    @Override
    public boolean setStartDate(String startDate) {
        final Pattern startDatePattern =
                Pattern.compile("((?:19|20)\\d\\d)/(0?[1-9]|1[012])/([12][0-9]|3[01]|0?[1-9])");

        if (startDate.length() != 10 || !startDatePattern.matcher(startDate).matches())
        {
            System.out.println("Please enter your start-date as YYYY/MM/DD with slashes");
            return false;
        }
        if (startDate == null)
        {
            System.out.println("Please enter your start-date as YYYY/MM/DD with slashes");
            return false;
        }

        this.startDate = startDate;
        return true;
    }

    @Override
    public String getStartDate() {
        return this.startDate;
    }
}
