package assignment5;


import java.util.regex.Pattern;

/**
 * This is the Student class which extends the Person class
 * Created by Jorge Palacios on 10/2/2018.
 * @author Jorge Palacios
 */
public class Student extends Person {

    final Pattern ADDRESS_PATTERN = Pattern.compile("^[0-9A-za-z-',. ]+$");

    private String classStanding;
    private String permanentAddress;
    private double gpa;

    /**
     * This constructor for the student class
     * ensures the classStanding, permanentAddress,and gpa properties will default to
     * "N/A" for all String values and 0 for all int values if a Student object was created but no values were set.
     */
    public Student()
    {
        this.classStanding = "N/A";
        this.permanentAddress = "N/A";
        this.gpa = 0.0;
    }

    /**
     * This is the getter used to access the value
     * of classStanding set by a student. When called
     * it simply returns whatever value is assigned to classStanding.
     * @return classStanding
     */
    public String getClassStanding() {
        return classStanding;
    }

    /**
     * This is the setter used to take the user's input and assign a String value
     * to classStanding.
     * This setter takes the user input, compares the input to acceptable cases in a switch statement,
     * then validates it for acceptable responses.
     * If the user input is not accepted then the setter returns false.
     * @param classStanding - a string that holds the user's input to set class standing.
     * @return true if user input passes validation.
     */
    public boolean setClassStanding(String classStanding) {

        switch (classStanding.toLowerCase())
        {
            case "f":
                this.classStanding = "Freshmen";
                return true;
            case "s":
                this.classStanding = "Sophomore";
                return true;
            case "j":
                this.classStanding = "Junior";
                return true;
            case "g":
                this.classStanding = "Graduate";
                return true;
            default:
                System.out.println(	"\nF for Freshmen " +
                        "\nS for Sophomore " +
                        "\nJ for Junior " +
                        "\nG for Graduate");
                return false;
        }
    }

    /**
     * This is the getter used to access the value
     * of permanentAddress set by a student. When called
     * it simply returns whatever value is assigned to permanentAddress.
     * @return permanentAddress
     */
    public String getPermanentAddress() {
        return permanentAddress;
    }

    /**
     * This is the setter used to take the user's input and assign a String value
     * to permanentAddress.
     * This setter takes the user input, compares the input to the acceptable length of letters,
     * then validates it for acceptable responses.
     * If the user input is not accepted then the setter returns false.
     * @param permanentAddress - a string used to hold the user's permanent address to set it.
     * @return true if user input passes validation.
     */
    public boolean setPermanentAddress(String permanentAddress) {
        if(permanentAddress.length() < 10 || permanentAddress.length() > 300)
        {
            System.out.println("(Minimum 10 characters)");
            return false;
        }
        if(!ADDRESS_PATTERN.matcher(permanentAddress).matches())
        {
            System.out.println("Only enter numbers, Upper-case and lower-case letters, dashes, apostrophes, commas,\n" +
                    "dots, and spaces");
            return false;
        }
        this.permanentAddress = permanentAddress;
        return true;
    }

    /**
     * This is the getter used to access the value
     * of gpa of a student. When called
     * it simply returns whatever value is assigned to gpa.
     * @return gpa
     */
    public double getGpa() {
        return gpa;
    }

    /**
     * This is the setter used to take the user's input and assign a double value
     * to gpa.
     * This setter takes the user input, compares the input to the acceptable values,
     * then validates by making sure the input values is between 0 and 4.
     * If the user input is not accepted then the setter returns false.
     * @param gpa - a double that holds the user's input to set their gpa.
     * @return true if user input passes validation.
     */
    public boolean setGpa(double gpa) {

        if (gpa <0 || gpa > 4)
        {
            System.out.println("GPA must be between 0 and 4!");
            return false;
        }

        this.gpa = gpa;
        return true;
    }

    /**
     *This toString method overrides the super toString method and
     * concatenates the values of classStanding and permanentAddress
     * to the information already stored in the object.
     */

    public void setEmail()
    {
        String emailHolder = "";
        emailHolder = getFirstName()+"."+getLastName()+"@ubalt.edu";

        this.email = emailHolder;

    }

    public String toString(){

        String studentInfo = 	"\n**Student**" +
                "\nClass Standing: " + getClassStanding() +
                "\nPermanent Address: " + getPermanentAddress() +
                "\nGPA: " + getGpa();

        return super.toString() + studentInfo;

    }
}

