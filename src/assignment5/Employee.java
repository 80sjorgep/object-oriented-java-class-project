package assignment5;

/**
 * Created by Jorge Palacios on 10/19/2018.
 * This is an interface that is used to expand the functionality
 * of people who earn money through the university(Faculty, Staff, or Tutor).
 * @author Jorge Palacios
 */
public interface Employee {
    /**
     * @param salary will take the user input for salary which must be a positive number.
     * @return true as long as the input was a positive number, else will return false.
     */
    public boolean setSalary(double salary);

    /**
     * getSalary
     * @return the salary set by a user.
     */
    public double getSalary();

    /**
     * @param startDate will take in the start date input by the user in string format.
     * @return true as long as the start date was entered as yyyy/MM/dd
     */
    public boolean setStartDate(String startDate);

    /**
     * getStartDate
     * @return the start date set by the user as yyyy/MM/dd
     */
    public String getStartDate();
}
