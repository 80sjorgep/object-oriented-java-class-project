package assignment5;


import java.time.LocalDate;
import java.time.Period;
import java.util.regex.Pattern;

/**
 * Created by Jorge Palacios on 10/2/2018.
 * @author Jorge Palacios
 */
public class Person {

    //These patterns help compare user input to acceptable values
    final Pattern NAME_PATTERN = Pattern.compile("^[A-Za-z-' ]+$");
    final Pattern ID_PATTERN = Pattern.compile("[1-9][0-9][0-9][0-9][0-9]");
    final Pattern BIRTHDATE_PATTERN = Pattern.compile("((?:19|20)\\d\\d)/(0?[1-9]|1[012])/([12][0-9]|3[01]|0?[1-9])");
    final Pattern ADDRESS_PATTERN = Pattern.compile("^[0-9A-za-z-',. ]+$");

    private int id;
    private String firstName;
    private String lastName;
    private String birthDate;
    private String address;
    /**
     * email is protected because it has to be accessed when the setEmail variable is overridden
     * by subclasses that have a different email format
     */
    protected String email;
    private String age;
    protected String yearsOfEmployment = "N/A";
    protected String startDate = "N/A";

    /**
     * This constructor for the Person class
     * ensures the id, firstName, lastName, birthDate, address, email, and age properties
     * will default to "N/A" as a String values and 0 for numeric values if a Person
     * object was created but no values were set.
     */
    public Person(){
        this.id = 0;
        this.firstName = "N/A;";
        this.lastName = "N/A;";
        this.birthDate = "N/A;";
        this.address = "N/A;";
        this.email = "N/A";
        this.age = "N/A";
        this.email = "N/A;";
        this.age = "N/A";
    }

    /**
     * getter used to access the ID number of the user
     * @return An int containing the user's ID number
     */
    public int getId() {return id;}

    /**
     * Setter used to set the ID number of the user,
     * this setter compares the user's input to a pattern that requires
     * five digits with no leading 0s in order to return true.
     * If the input is not accepts the setter returns false.
     * @param id A int containing the user's input ID number.
     * @return true if user input passes validation.
     */
    public boolean setId(int id) {

        String idTempParsed = Integer.toString(id);

        if(!ID_PATTERN.matcher(idTempParsed).matches()){

            System.out.println("ID Num must not start with 0! and contain 5 digits");
            return false;
        }
        /*
        if(idTempParsed.length() != 5 ) {

            System.out.println("Please enter a 5 digit id number");
            return  false;
        }
        */
        this.id = id;
        return true;
    }

    /**
     * getter used to access the first name of the user
     * @return A String containing the user's first name
     */
    public String getFirstName() {return firstName;}

    /**
     * Setter used to set the first name of the user.
     * This setters compares the user input to an acceptable pattern
     * for validation. If the input does not validate then the setter
     * returns false.
     * @param firstName A String containing the user's first name
     * @return true if user input passes validation.
     */
    public boolean setFirstName(String firstName) {

        if(firstName.length() < 2 ||firstName.length() > 50)
        {
            System.out.println("First Name length = "+firstName.length()+" \nMin 2 letters, max 50 letters");
            return false;
        }
        if (!NAME_PATTERN.matcher(firstName).matches() || firstName == null) {

            System.out.println("Only Upper-case and lower-case letters, dashes, apostrophes, and spaces are accepted!");

            return false;
        }

        this.firstName = firstName;
        setEmail();
        return true;
    }

    /**
     * getter used to access the last name of the user
     * @return A String containing the user's last name
     */
    public String getLastName() {return lastName;}

    /**
     * Setter used to set the last name of the user
     * @param lastName A String containing the user's last name.
     * @return true if user input passes validation.
     */
    public boolean setLastName(String lastName) {

        if (lastName.length() < 2|| lastName.length() > 50) {
            System.out.println("Character length = "+lastName.length());
            return false;
        }
        if (!NAME_PATTERN.matcher(lastName).matches()) {

            System.out.println("Only Upper-case and lower-case letters, dashes, apostrophes, and spaces are accepted!");

            return false;
        }

        this.lastName = lastName;
        setEmail();
        return true;
    }

    /**
     * getter used to access the birthday of the user
     * @return A String containing the user's birthday
     */
    public String getBirthDate() {return birthDate;}

    /**
     * Setter used to set the birthday of the user
     * @param birthDate A String containing the user's birthday.
     * @return true if user input passes validation.
     */
    public boolean setBirthDate(String birthDate) {

        this.birthDate = birthDate;

        if (birthDate.length() != 10 || !BIRTHDATE_PATTERN.matcher(birthDate).matches()) {

            return false;
        }

        boolean ageIsOver1 = setAge();

        if(ageIsOver1)
        {
            this.birthDate = birthDate;
            return true;
        }
        else
        {
            System.out.println("Please enter a correct birth-date that is older than today's date!");
            return false;
        }
    }
    /**
     * Setter used to set the yearsOfEmployment for users it applies to
     * @param startDate A String containing the user's birthday.
     * @return true if user input passes validation.
     */

    /**
     * getter used to access the address of the user
     * @return A String containing the user's address
     */
    public String getAddress() {return address;}

    /**
     * Setter used to set the address of the user
     * @param address A String containing the user's address.
     * @return true if user input passes validation.
     */
    public boolean setAddress(String address) {

        if(address.length() < 10 || address.length() > 300)
        {
            System.out.println("(Minimum 10 characters)");
            return false;
        }
        if(!ADDRESS_PATTERN.matcher(address).matches())
        {
            System.out.println("Only enter numbers, Upper-case and lower-case letters, dashes, apostrophes, commas,\n" +
                    "dots, and spaces");
            return false;
        }
        this.address = address;
        return true;
    }

    /**
     * User does not have to access the getter and setter for email address and birth date, these methods will be called
     * without asking the user for input
     * setEmail is called later to concatenate the last and first name to generate an email with the format
     * firstName.lastName@ubalt.edu at the super level but will be overridden at the student level.
     */
    public void setEmail()
    {
        String emailHolder = "";
        String initialHolder = "";

        /**
         * this loop grabs the first initial of the first name
         * then assigns the value to intialHolder
         * which will be concatenated to the beginning of the email
         */

        for (int i =0; i < 1; i++ )
        {
            initialHolder = initialHolder + getFirstName().charAt(i);
        }

        emailHolder = initialHolder + "."+getLastName()+"@ubalt.edu";

        this.email = emailHolder;
    }

    /**
     * getter used to access the email of the user
     * @return email - A String containing the user's email
     */
    private final String getEmail()
    {
        return email;
    }

    /**
     * A private setter that gets called once there is a birth-date
     * This method takes the user's birth-date, loops through it, assigning the
     * year, month, and day to their separate String variables. Then those variables are
     * parsed into integers which are then used to calculate the age with the Period.between function.
     */
    public final boolean setAge()
    {
        /**
         * these are string place holders for the birth-date year, month, and day
         * that will be converted to ints later in the method
         */
        String  birthDateYear = "",
                birthDateMonth = "",
                birthDateDay = "";

        //loop to grab the birthDateYear
        for(int i = 0; i < 4; i++){
            //date entered as a String formatted "yyyy/MM/dd"
            birthDateYear = birthDateYear + getBirthDate().charAt(i);
        }

        //loop to grab the birthDateMonth
        for(int i = 5; i < 7; i++){
            //date entered as a String formatted "yyyy/MM/dd"
            birthDateMonth = birthDateMonth + getBirthDate().charAt(i);
        }

        //loop to grab the birthDateDay
        for(int i = 8; i < 10; i++){
            //date entered as a String formatted "yyyy/MM/dd"
            birthDateDay = birthDateDay + getBirthDate().charAt(i);
        }

        //These temp variables hold the data from the loops above to calculate age
        LocalDate currentDate = LocalDate.now();
        LocalDate birthday = LocalDate.of(  Integer.parseInt(birthDateYear),
                Integer.parseInt(birthDateMonth),
                Integer.parseInt(birthDateDay));

        //Period.between() will calculate the years between 2 dates input
        Period p = Period.between(birthday, currentDate);

        //This will change this.birthDate to a yyyy-MM-dd as opposed to yyyy/MM/dd
        this.birthDate = birthday.toString();
        //Setting the age after calculating the difference in year's from today's date and the user's b-day
        if (p.getYears() > 1)
        {
            this.age = p.getYears() + " years old";
            return true;
        }
        else
        {
            System.out.println("System generated currentDate = " + currentDate);
            System.out.println("birthDate entered = " + birthday);
            this.age = "Incorrect B-Day entered, cannot have a negative age!";
            return false;
        }
    }

    /**
     * A private setter that gets called once there is a start-date
     * This method takes the user's start-date, loops through it, assigning the
     * year, month, and day to their separate String variables. Then those variables are
     * parsed into integers which are then used to calculate the years of employment for those
     * who are employed. Period.between function.
     */
    public final boolean setYearsOfEmployment()
    {
        /**
         * these are string place holders for the birth-date year, month, and day
         * that will be converted to ints later in the method
         */
        String  startDateYear = "",
                startDateMonth = "",
                startDateDay = "";

        //loop to grab the startDateYear
        for(int i = 0; i < 4; i++){
            //date entered as a String formatted "yyyy/MM/dd"
            startDateYear = startDateYear + getStartDate().charAt(i);
        }

        //loop to grab the startDateMonth
        for(int i = 5; i < 7; i++){
            //date entered as a String formatted "yyyy/MM/dd"
            startDateMonth = startDateMonth + getStartDate().charAt(i);
        }

        //loop to grab the startDateDay
        for(int i = 8; i < 10; i++){
            //date entered as a String formatted "yyyy/MM/dd"
            startDateDay = startDateDay + getStartDate().charAt(i);
        }

        //These temp variables hold the data from the loops above to calculate age
        LocalDate currentDate = LocalDate.now();
        LocalDate startDate = LocalDate.of(  Integer.parseInt(startDateYear),
                Integer.parseInt(startDateMonth),
                Integer.parseInt(startDateDay));

        //Period.between() will calculate the years between 2 dates input
        Period p = Period.between(startDate, currentDate);

        //This will change this.birthDate to a yyyy-MM-dd as opposed to yyyy/MM/dd
        this.startDate = startDate.toString();
        //Setting the age after calculating the difference in year's from today's date and the user's start-day
        if (p.getYears() > 1)
        {
            this.yearsOfEmployment = p.getYears() + " years";
            return true;
        }
        else
        {
            System.out.println("System generated currentDate = " + currentDate);
            System.out.println("startDate entered = " + startDate);

            this.yearsOfEmployment =    "Incorrect employment start date entered, +" +
                    "\ncannot have work a total of negative years!";
            return false;
        }
    }

    /**
     * getter used to access the age of the user
     * @return age - A String containing the user's age
     */
    private final String getAge(){

        return this.age;
    }

    /**
     * This toString method overrides java's standard to string method and is formatted to
     * present all the properties that make up a Person object and also sets the stage
     * so other classes that extend the Person class expand on this toString method
     * once overridden
     * @return id
     * @return firstName
     * @return lastName
     * @return birthDate
     * @return address
     * @return email
     * @return age
     */
    public String toString(){

        String personData = "id # "+ Integer.toString(getId()) +
                "\nFirst Name: " + getFirstName() +
                "\nLast Name: " +  getLastName() +
                "\nBirth Date: " + getBirthDate() +
                "\nAddress: " + getAddress() +
                "\nEmail: " + getEmail() +
                "\nAge = "+ getAge();

        return personData;
    }

    public String getStartDate()
    {
        return this.startDate;
    }
}