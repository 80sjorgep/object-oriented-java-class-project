package assignment5;

import javafx.beans.binding.ObjectExpression;
import sun.rmi.server.InactiveGroupException;

import java.util.Scanner;

/**
 * Created by Jorge Palacios on 11/26/2018
 * @author Jorge Palacios
 * @see Person
 * @see Staff
 * @see Faculty
 * @see FullTime
 * @see PartTime
 * @see Student
 * @see Tutor
 * @see Employee
 */
public class Assignment5 {

    private static SimpleArrayList<Person> universtitySimpleArrayList =
            new SimpleArrayList<>();


    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        boolean closeEntry = false;
        boolean incorrectEntry = false;


        //This loop allows the user to make a selection within a switch-statement
        do
        {
            closeEntry = false;

            System.out.println(
                    "Enter 0) To exit and close:\n" +
                    "Enter 1) To create and add a new person to the list:\n"+
                    "Enter 2) Show the information stored for each person:\n"+
                    "Enter 3) To edit the information stored in the list: \n"+
                    "Enter 4) To remove a person stored in the list: \n"+
                    "Enter 5) To add a new person to the list at a specific position:\n"+
                    "Enter 6) To create and add 5 person objects to the list:");
            int notNullCount = 0;
            switch (input.nextLine())
            {
                case "0":
                    input.close();
                    return;
                case "1":

                    do {
                        System.out.println("Please Enter" +
                                "\n1 for Staff"+
                                "\n2 for Faculty"+
                                "\n3 for Student");
                    }while (addPerson(input.nextLine(), universtitySimpleArrayList) == false);
                    System.out.println("PERSON ADDED SUCCESSFULLY VIA SIMPLE ARRAY LIST!!!!!");
                    break;

                case "2":

                    if(universtitySimpleArrayList.size() > 0)
                    {
                        for (int i = 0; i < universtitySimpleArrayList.size(); i++)
                        {
                            System.out.println("----------------------------------------------");

                            //Here we cast the Person class in order to access all the person methods
                            Person personToDisplay = (Person)universtitySimpleArrayList.get(i);
                            System.out.println(
                                    (i+1)+") ID# "+
                                    personToDisplay.getId() + " Name: " +
                                    personToDisplay.getFirstName() +" "+ personToDisplay.getLastName());

                            System.out.println("----------------------------------------------");
                        }


                        do {
                            try
                            {
                                incorrectEntry = false;

                                System.out.println("Select a person to retrieve their information.");

                                int indexOfPersonToSelect = Integer.parseInt(input.nextLine());

                                if(indexOfPersonToSelect < 1 ||
                                    indexOfPersonToSelect > universtitySimpleArrayList.size())
                                {
                                    System.out.println("Incorrect selection");
                                    incorrectEntry = true;
                                }

                                System.out.println(universtitySimpleArrayList.get(indexOfPersonToSelect - 1).toString());
                                System.out.println("----------------------------------------------");
                            }
                            catch (NumberFormatException e)
                            {
                                incorrectEntry = true;
                                System.out.println("Please enter a number between 1 and " +
                                        (universtitySimpleArrayList.size()));
                                System.out.println(e.getMessage());
                            }
                        } while (incorrectEntry);

                    }else {
                        System.out.println("There are no people stored in the list!");
                    }

                    break;
                case "3":

                    if(universtitySimpleArrayList.size() > 0)
                    {
                        for (int i = 0; i < universtitySimpleArrayList.size(); i++)
                        {
                            System.out.println("----------------------------------------------");

                            //Here we cast the Person class in order to access all the person methods
                            Person personToDisplay = (Person)universtitySimpleArrayList.get(i);
                            System.out.println(
                                    (i+1)+") ID# "+
                                            personToDisplay.getId() + " Name: " +
                                            personToDisplay.getFirstName() +" "+ personToDisplay.getLastName());

                            System.out.println("----------------------------------------------");
                        }


                        do {
                            try
                            {
                                incorrectEntry = false;

                                System.out.println("Select a person to edit their information.");

                                int indexOfPersonToSelect = Integer.parseInt(input.nextLine());

                                if(indexOfPersonToSelect < 1 ||
                                        indexOfPersonToSelect > universtitySimpleArrayList.size())
                                {
                                    System.out.println("Incorrect selection");
                                    incorrectEntry = true;
                                }else
                                {
                                editPersonFromList((Person) universtitySimpleArrayList.get(indexOfPersonToSelect - 1),
                                                    input);
                                System.out.println("----------------------------------------------");
                                }
                            }
                            catch (NumberFormatException e)
                            {
                                incorrectEntry = true;
                                System.out.println("Please enter a number between 1 and " +
                                        (universtitySimpleArrayList.size()));
                                System.out.println(e.getMessage());
                            }
                        } while (incorrectEntry);

                    }else {
                        System.out.println("There are no people stored in the list!");
                    }

                    break;
                case "4":
                    if(universtitySimpleArrayList.size() > 0)
                    {
                        for (int i = 0; i < universtitySimpleArrayList.size(); i++)
                        {
                            System.out.println("----------------------------------------------");

                            //Here we cast the Person class in order to access all the person methods
                            Person personToDisplay = (Person)universtitySimpleArrayList.get(i);
                            System.out.println(
                                    (i+1)+") ID# "+
                                            personToDisplay.getId() + " Name: " +
                                            personToDisplay.getFirstName() +" "+ personToDisplay.getLastName());

                            System.out.println("----------------------------------------------");
                        }


                        do {
                            try
                            {
                                incorrectEntry = false;

                                System.out.println("Select a person to removed from the list.");

                                int indexOfPersonToSelect = Integer.parseInt(input.nextLine());

                                if(indexOfPersonToSelect < 1 ||
                                        indexOfPersonToSelect > universtitySimpleArrayList.size())
                                {
                                    System.out.println("Incorrect selection");
                                    incorrectEntry = true;
                                }

                                Person tempPersonToDisplay = new Person();
                                boolean removalIsSuccess = true;

                                try
                                {
                                    removalIsSuccess = true;
                                    tempPersonToDisplay =  (Person) universtitySimpleArrayList.get(indexOfPersonToSelect - 1);
                                }
                                catch (Exception e)
                                {
                                    removalIsSuccess = false;
                                    System.out.println("Error/Index or casting "+e.getMessage());
                                }
                                //if we successfully casted we can continue
                                if(removalIsSuccess)
                                {
                                    System.out.println("Removing " + tempPersonToDisplay.getFirstName() + " " + tempPersonToDisplay.getLastName());
                                }else
                                {
                                    System.out.println("Unsuccessful removal attempt, please enter correct number!");
                                }

                                try
                                {
                                    universtitySimpleArrayList.remove(indexOfPersonToSelect );
                                }
                                catch (Exception e)
                                {
                                    removalIsSuccess = false;
                                    System.out.println("Index Error: "+e.getMessage());
                                }
                                if(removalIsSuccess)
                                {
                                    //we do not have to deduct 1 from the input as that is already handled in
                                    //the SimpleArrayRemoveMethod
                                    System.out.println("----- "+(indexOfPersonToSelect)+" Removal Complete ---------");
                                }
                            }
                            catch (NumberFormatException e)
                            {
                                incorrectEntry = true;
                                System.out.println("Please enter a number between 1 and " +
                                        (universtitySimpleArrayList.size()));
                                System.out.println(e.getMessage());
                            }
                        } while (incorrectEntry);

                    }else {
                        System.out.println("There are no people stored in the list!");
                    }
                    break;

                case "5":

                    do {
                        System.out.println("Please Enter" +
                                "\n1 for Staff"+
                                "\n2 for Faculty"+
                                "\n3 for Student");
                    }while (addPersonAtIndex(input.nextLine(), universtitySimpleArrayList) == false);

                    break;
                case "6":
                    //This case is simply for testing to help speed up the process and create an Object for each class
                    Person[] testPersonArray = new Person[5];
                    testPersonArray = CreateArrayOfEachObj(testPersonArray,testPersonArray.length );

                    System.out.println("Adding a temp array for testing...");

                    for (Person person:testPersonArray)
                    {
                        universtitySimpleArrayList.add(person);
                        System.out.println("Added a " + person.getFirstName());
                    }

                    System.out.println("Testing complete");
                    break;
                default:
                    closeEntry = false;
                    break;
            }
        } while (!closeEntry);
        //After a successful loop through the switch statement the user will be returned to the main menu
    }


    /**
     *{@link #addPerson(String, SimpleArrayList)}
     * This method allows the user to select whether they're faculty student or staff.
     * If the user does not select the correct option the switch statement loops until
     * asking the user to select faculty student or staff.
     * Once the selects faculty student or staff, the 'createPersonData' function gets called and passes
     * the correct object created according to user input
     */
    static boolean addPerson (String staffFacultyStudent, SimpleArrayList simpleArrayList)
    {
        Scanner input = new Scanner(System.in);

        switch (staffFacultyStudent) {
            case "1":
                System.out.println("You have selected staff");
                Staff newStaff = new Staff();
                createPersonData(newStaff);
                System.out.println("----New Profile Created!---");
                System.out.println(newStaff.toString());
                simpleArrayList.add(newStaff);
                return true;
            case "2":
                String inputFTorPT = "N/A";
                boolean userDidInput = false;
                //This print line confirms the users selection of faculty
                System.out.println("You have selected faculty");

                while (userDidInput == false) {
                    //This print line ask users to input full-time or part-time status
                    System.out.println("Please enter " +
                            "\n1 for Full-Time or 2 for Part-Time?");

                    inputFTorPT = input.nextLine();

                    switch (inputFTorPT) {
                        case "1":
                            System.out.println("You have selected Full-Time!");
                            FullTime newFullTime = new FullTime();
                            createPersonData(newFullTime);
                            System.out.println("----New Profile Created!---");
                            System.out.println(newFullTime.toString());
                            simpleArrayList.add(newFullTime);
                            return true;
                        case "2":
                            System.out.println("You have selected Part-Time!");
                            PartTime newPartTime = new PartTime();
                            createPersonData(newPartTime);
                            System.out.println("----New Profile Created!---");
                            System.out.println(newPartTime.toString());
                            simpleArrayList.add(newPartTime);
                            return true;
                        default:
                            userDidInput = false;
                            //break;
                    }
                }
            case "3":
                System.out.println("You have selected student" +
                        "\nAre you also a Tutor?");
                userDidInput = false;

                while (!userDidInput) {
                    System.out.println("Input Y for yes and N for no!");
                    switch (input.nextLine().toLowerCase()) {
                        case "y":
                            Tutor newTutor = new Tutor();
                            createPersonData(newTutor);
                            System.out.println("----New Profile Created!---");
                            System.out.println(newTutor.toString());
                            simpleArrayList.add(newTutor);
                            return true;
                        case "n":
                            Student newStudent = new Student();
                            createPersonData(newStudent);
                            System.out.println("----New Profile Created!---");
                            System.out.println(newStudent.toString());
                            simpleArrayList.add(newStudent);
                            return true;
                        default:
                            userDidInput = false;
                    }
                }
            default:
                System.out.println("Please Enter" +
                        "\n1 for Staff" +
                        "\n2 for Faculty" +
                        "\n3 for Student");
                return false;
        }
    }

    /**
     *{@link #addPersonAtIndex(String, SimpleArrayList)}
     * This method allows the user to select whether they're faculty student or staff.
     * If the user does not select the correct option the switch statement loops until
     * asking the user to select faculty student or staff.
     * Once the selects faculty student or staff, the 'createPersonData' function gets called and passes
     * the correct object created according to user input.
     * This method also allows the user to select which index they would like to add a new person in the list
     */
    static boolean addPersonAtIndex (String staffFacultyStudent, SimpleArrayList simpleArrayList)
    {
        Scanner input = new Scanner(System.in);
        int indexToPlaceNewAddition;
        boolean correctIndexSelected;

        switch (staffFacultyStudent) {
            case "1":
                System.out.println("You have selected staff");
                Staff newStaff = new Staff();
                createPersonData(newStaff);

                System.out.println("Which index position would you like to add the added person?");

                do {
                    correctIndexSelected = true;
                    indexToPlaceNewAddition = 0;
                    try
                    {
                        indexToPlaceNewAddition = Integer.parseInt(input.nextLine());

                    } catch (NumberFormatException e) {
                        correctIndexSelected = false;
                        System.out.println("Error, please enter an available slot " + e.getMessage());
                    }
                } while (!correctIndexSelected);

                System.out.println(newStaff.toString());
                //Deducting one from the entered index position so the user does not have to worry about subzero index
                return simpleArrayList.add(newStaff,indexToPlaceNewAddition - 1);
                //return true;
            case "2":
                String inputFTorPT = "N/A";
                boolean userDidInput = false;
                //This print line confirms the users selection of faculty
                System.out.println("You have selected faculty");

                while (userDidInput == false) {
                    //This print line ask users to input full-time or part-time status
                    System.out.println("Please enter " +
                            "\n1 for Full-Time or 2 for Part-Time?");

                    inputFTorPT = input.nextLine();

                    switch (inputFTorPT) {
                        case "1":
                            System.out.println("You have selected Full-Time!");
                            FullTime newFullTime = new FullTime();
                            createPersonData(newFullTime);

                            System.out.println("Which index position would you like to add the added person?");

                            do {
                                correctIndexSelected = true;
                                indexToPlaceNewAddition = 0;
                                try
                                {
                                    indexToPlaceNewAddition = Integer.parseInt(input.nextLine());

                                } catch (NumberFormatException e) {
                                    correctIndexSelected = false;
                                    System.out.println("Error, please enter an available slot " + e.getMessage());
                                }
                            } while (!correctIndexSelected);

                            System.out.println(newFullTime.toString());
                            //Deducting one from the entered index position so the user does not have to worry about subzero index
                            return simpleArrayList.add(newFullTime,indexToPlaceNewAddition - 1);
                            //return true;
                        case "2":
                            System.out.println("You have selected Part-Time!");
                            PartTime newPartTime = new PartTime();
                            createPersonData(newPartTime);

                            System.out.println("Which index position would you like to add the added person?");

                            do {
                                correctIndexSelected = true;
                                indexToPlaceNewAddition = 0;
                                try
                                {
                                    indexToPlaceNewAddition = Integer.parseInt(input.nextLine());

                                } catch (NumberFormatException e) {
                                    correctIndexSelected = false;
                                    System.out.println("Error, please enter an available slot " + e.getMessage());
                                }
                            } while (!correctIndexSelected);

                            System.out.println(newPartTime.toString());
                            //Deducting one from the entered index position so the user does not have to worry about subzero index
                            return simpleArrayList.add(newPartTime,indexToPlaceNewAddition - 1);
                            //return true;
                        default:
                            userDidInput = false;
                            //break;
                    }
                }
            case "3":
                System.out.println("You have selected student" +
                        "\nAre you also a Tutor?");
                userDidInput = false;

                while (!userDidInput) {
                    System.out.println("Input Y for yes and N for no!");
                    switch (input.nextLine().toLowerCase()) {
                        case "y":
                            Tutor newTutor = new Tutor();
                            createPersonData(newTutor);

                            System.out.println("Which index position would you like to add the added person?");

                            do {
                                correctIndexSelected = true;
                                indexToPlaceNewAddition = 0;
                                try
                                {
                                    indexToPlaceNewAddition = Integer.parseInt(input.nextLine());

                                } catch (NumberFormatException e) {
                                    correctIndexSelected = false;
                                    System.out.println("Error, please enter an available slot " + e.getMessage());
                                }
                            } while (!correctIndexSelected);

                            System.out.println(newTutor.toString());
                            //Deducting one from the entered index position so the user does not have to worry about subzero index
                            return simpleArrayList.add(newTutor,indexToPlaceNewAddition - 1);
                            //return true;
                        case "n":
                            Student newStudent = new Student();
                            createPersonData(newStudent);

                            System.out.println("Which index position would you like to add the added person?");

                            do {
                                correctIndexSelected = true;
                                indexToPlaceNewAddition = 0;
                                try
                                {
                                    indexToPlaceNewAddition = Integer.parseInt(input.nextLine());

                                } catch (NumberFormatException e) {
                                    correctIndexSelected = false;
                                    System.out.println("Error, please enter an available slot " + e.getMessage());
                                }
                            } while (!correctIndexSelected);

                            System.out.println(newStudent.toString());
                            //Deducting one from the entered index position so the user does not have to worry about subzero index
                            return simpleArrayList.add(newStudent,indexToPlaceNewAddition - 1);
                            //return true;
                        default:
                            userDidInput = false;
                    }
                }
            default:
                System.out.println("Please Enter" +
                        "\n1 for Staff" +
                        "\n2 for Faculty" +
                        "\n3 for Student");
                return false;
        }
    }

    /**
     * {@link #createPersonData(Person)}
     * This method takes in user input to create a person in the system.
     * Depending on which option the user selected, staff, faculty, or student,
     * the method will ask the appropriate questions relevant to the option picked.
     */
    static void createPersonData(Person personTest){
        Scanner input = new Scanner(System.in);

        boolean errorIsTrue = false;
        do {
            errorIsTrue = false;
            try{
                System.out.println("Please enter your 5 digit ID number");
                do {

                }while (personTest.setId(Integer.parseInt(input.nextLine())) == false);
            }catch (Exception e){
                errorIsTrue = true;
                System.out.println("Must input 5 digit ID number! ERROR " + e.getMessage().toString());
            }
        }while (errorIsTrue);

        System.out.println("Please enter your first name");
        do {
            System.out.println("Please enter between 2 and 50 letters");
        }while (personTest.setFirstName(input.nextLine()) == false);

        System.out.println("Please enter your last name");
        do {
            System.out.println("Please enter between 2 and 50 letters");
        }while (personTest.setLastName(input.next()) == false);


        System.out.println("Please enter your birth-date as YYYY/MM/DD with slashes");
        do {

        }while (personTest.setBirthDate(input.next()) == false);

        System.out.println("Please enter your address");
        do {

        }while (personTest.setAddress(input.nextLine()) == false);

        /**
         * From here on out the system will only ask for user inputs related to their selection
         */
        if (personTest instanceof Staff)
        {
            System.out.println("Enter department name!");
            do {

            }while (((Staff) personTest).setDepartment(input.nextLine()) == false);

            System.out.println("Enter supervisor name!");
            do {

            }while (((Staff) personTest).setSupervisor(input.nextLine()) == false);
        }

        if(personTest instanceof FullTime || personTest instanceof PartTime)
        {
            //The user will input their division
            System.out.println("Please enter your division");
            do{

            }
            while (((Faculty) personTest).setDivision(input.nextLine()) == false);

            //The user will input their degree
            System.out.println("Please enter your degree");
            System.out.println("Enter" +
                    "\nB for Bachelor's"+
                    "\nM for Master's"+
                    "\nD for Doctorate");
            do{

            }
            while (((Faculty) personTest).setDegree(input.nextLine()) == false);

            do
            {
                errorIsTrue = false;
                try
                {
                    //The user will input their graduation year
                    System.out.println("Please enter your year of graduation");
                    System.out.println("Must be between 1950 and current year!");
                    do{

                    }
                    while (((Faculty) personTest).setYearOfGraduation(Integer.parseInt(input.nextLine())) == false);
                }
                catch (Exception e)
                {
                    System.out.println("ERROR! " + e.getMessage());
                    errorIsTrue = true;
                }

            }while (errorIsTrue);
        }
        //This if statement will allow for full-time faculty to input their rank
        if (personTest instanceof  FullTime)
        {
            //The user will input their rank
            System.out.println("Please enter your rank");
            System.out.println( "\n1 for Lecturer" +
                    "\n2 for Assistant Professor" +
                    "\n3 for Associate Professor" +
                    "\n4 for Full Professor");
            do {

            }while (((FullTime) personTest).setRank(input.nextLine()) == false);
        }
        //This if statement will allow for part-time faculty to input their number of courses taught
        if(personTest instanceof PartTime)
        {
            do
            {
                errorIsTrue = false;
                try
                {
                    //The user will input their rank
                    System.out.println("Please enter the number of courses taught(between 1 and 100)");
                    do {

                    }while (((PartTime) personTest).setCoursesTaught(input.nextLine()) == false);
                }
                catch (Exception e)
                {
                    errorIsTrue = true;
                    System.out.println("ERROR! "+e.getMessage());
                }
            }while (errorIsTrue);

        }
        //This if statement will handle student input
        if(personTest instanceof Student || personTest instanceof Tutor)
        {
            System.out.println("Please enter class standing!");
            System.out.println("\nF for Freshmen \nS for Sophomore \nJ for Junior \nS for Senior \nG for Graduate\n");
            do {

            } while (((Student) personTest).setClassStanding(input.nextLine()) == false);
            //This do while loop will allow students to enter their gpa

            System.out.println("Is " + personTest.getAddress() +
                    "\nYour Permanent Address? ");

            do {
                errorIsTrue = false;
                System.out.println("Please enter Y for yes or N for no!");
                switch (input.nextLine().toLowerCase())
                {
                    case "y":
                        do {

                        }while (((Student) personTest).setPermanentAddress(personTest.getAddress()) == false);
                        break;
                    case "n":
                        System.out.println("Please enter your permanent address!");
                        do {

                        }while (((Student) personTest).setPermanentAddress(input.nextLine()) == false);
                        break;
                    default:
                        errorIsTrue = true;
                        break;
                }
            } while (errorIsTrue);


            do {
                errorIsTrue = false;
                try {
                    System.out.println("Please enter your GPA!");
                    do {

                    } while (((Student) personTest).setGpa(Double.parseDouble(input.nextLine())) == false);
                } catch (Exception e) {
                    errorIsTrue = true;
                    System.out.println("ERROR! " + e.getMessage() +
                            "\nMust Enter A DIGIT between 0 and 4!");
                }
            } while (errorIsTrue);
        }

        if (personTest instanceof Tutor)
        {
            System.out.println("Enter topics tutored");
            do {

            }while (((Tutor) personTest).setTopics(input.nextLine()) == false);

            do{
                errorIsTrue = false;
                System.out.println("Enter number of hours tutored");
                try {
                    do {

                    }while (((Tutor) personTest).setHours(Double.parseDouble(input.nextLine())) == false);
                }catch (Exception e){
                    System.out.println("ERROR! "+e.getMessage());
                    errorIsTrue = true;
                }
            } while (errorIsTrue);
        }
        //Here the user will be asked for the newly added information(Start Date and Salary)
        if (personTest instanceof Faculty||personTest instanceof Staff ||personTest instanceof Tutor)
        {
            do {
                try
                {
                    System.out.println("Enter your start-date (yyyy/MM/dd):");
                }
                catch (Exception e){
                    System.out.println("Error! " + e.getMessage());
                }
            }
            //Since these classes implement the same interface the interface can be
            //casted to any classes that implement it.
            while (((Employee) personTest).setStartDate(input.nextLine()) == false);

            do{
                errorIsTrue = false;
                try
                {
                    do
                    {
                        System.out.println("Enter your salary, must be entered as numeric amount!");
                    }while (((Employee) personTest).setSalary(Double.parseDouble(input.nextLine())) == false);
                }
                catch (Exception e)
                {
                    System.out.println("ERROR! "+e.getMessage());
                    errorIsTrue = true;
                }
            } while (errorIsTrue);
        }

        /**
         * these methods are called at the end of the function because
         * the user does not input any data directly into the methods
         * they are simply called to generate the correct email format
         * and calculate the age.
         */
        //Creating email with first and last name collected from user input
        //personTest.setEmail();
        //calculating age with birthday info from user input
        personTest.setAge();
    }

    /**
     * {@link #editPersonFromList(Person, Scanner)}
     * This method takes our selected 'Person' from our simpeArrayList when we select option number 3.
     * The user can then edit the object which then get returns with the updated information.
     * The object selected gets updated via teh editStaffFromList method.
     * This method is just one in many that's executed when the object relating to the
     * specific method is selected. For example, if a student object is passed then the
     * editStudentFromList method is executed. There is a specific method for each
     * possible 'Person' object.
     */
    static Person editPersonFromList(Person personFromList, Scanner input)
    {
        System.out.println("You have executed the editPersonFromList() function");

        try
        {
            if(personFromList instanceof Staff)
            {
                editStaffFromList(personFromList, input);
                return personFromList;
            }
            if(personFromList instanceof FullTime)
            {
                editFacultyFullTimeFromList(personFromList, input);
                return personFromList;
            }
            if(personFromList instanceof PartTime)
            {
                editFacultyPartTimeFromList(personFromList, input);
                return personFromList;
            }
            if(personFromList instanceof Tutor)
            {
                System.out.println("This is an instance of Tutor");
                editTutorFromList(personFromList, input);
                return personFromList;
            }
            if(personFromList instanceof Student)
            {
                System.out.println("This is an instance of Student");
                editStudentFromList(personFromList, input);
                return personFromList;
            }
        } catch (Exception e) {
            System.out.println("Error "+e.getMessage());
            e.printStackTrace();
        }
        return personFromList;
    }

    /**
     * {@link #editPerson(Person[], Scanner)}
     * This method takes our array of size 5 which holds our objects then loops through it
     * so the user can select an object to edit then returns an array of Person[].
     * Our main array holding our objects will be replaced with the one returned from
     * this method.
     */
    static Person[] editPerson(Person[] personArray, Scanner input)
    {
        System.out.println("You have executed the editPerson() function");
        int indexSelection = 0;
        boolean incorrectIndexSelection = false;
        int numberOfPeopleInList = 0;

        //These loop here creates the selection menu
        System.out.println("Select a person to edit by entering their number");
        System.out.println("0) Back:");
        for (int i = 0; i < personArray.length; i++)
        {
            if(personArray[i] != null)
            {
                numberOfPeopleInList++;

                System.out.println((i+1) + ") " +   personArray[i].getFirstName() + " " +
                        personArray[i].getLastName() + " ID # " +
                        personArray[i].getId());
            }
        }

        do
        {
            try {
                System.out.println("Please enter a number between 1 and " + numberOfPeopleInList);
                indexSelection = Integer.parseInt(input.nextLine());
            }catch (Exception e){System.out.println("Error " + e.getMessage());}
            System.out.println("indexSelectionVal = " + indexSelection);

            if (indexSelection == 0){ return personArray; }

            if (indexSelection < 0 || indexSelection > numberOfPeopleInList)
            { incorrectIndexSelection = true; } else { incorrectIndexSelection = false; }
        }
        while (incorrectIndexSelection);

        indexSelection -= 1;

        System.out.println("indexSelectionVal = " + indexSelection);

        try
        {
            if(personArray[indexSelection] instanceof Staff)
            {
                editStaff(personArray, indexSelection, input);
                return personArray;
            }
            if(personArray[indexSelection] instanceof FullTime)
            {
                editFacultyFullTime(personArray, indexSelection, input);
                return personArray;
            }
            if(personArray[indexSelection] instanceof PartTime)
            {
                editFacultyPartTime(personArray, indexSelection, input);
                return personArray;
            }
            if(personArray[indexSelection] instanceof Tutor)
            {
                System.out.println("This is an instance of Tutor");
                editTutor(personArray, indexSelection, input);
                return personArray;
            }
            if(personArray[indexSelection] instanceof Student)
            {
                System.out.println("This is an instance of Student");
                editStudent(personArray, indexSelection, input);
                return personArray;
            }
        } catch (Exception e) {
            System.out.println("Error "+e.getMessage());
            e.printStackTrace();
        }
        return personArray;
    }

    static void selectPersonToView(Person[] personArray, Scanner input)
    {
        int indexSelection = 0;
        boolean incorrectIndexSelection = false;
        int numberOfPeopleInList = 0;

        //These loop here creates the selection menu
        System.out.println("Select a person to view by entering their number");
        System.out.println("0) Back:");
        for (int i = 0; i < personArray.length; i++)
        {
            if(personArray[i] != null)
            {
                numberOfPeopleInList++;

                System.out.println((i+1) + ") " +   personArray[i].getFirstName() + " " +
                        personArray[i].getLastName() + " ID # " +
                        personArray[i].getId());
            }
        }

        do
        {
            try {
                System.out.println("Please enter a number between 1 and " + numberOfPeopleInList);
                indexSelection = Integer.parseInt(input.nextLine());
            }catch (Exception e){System.out.println("Error " + e.getMessage());}
            System.out.println("indexSelectionVal = " + indexSelection);

            if (indexSelection == 0){ return; }

            if (indexSelection < 0 || indexSelection > numberOfPeopleInList)
            { incorrectIndexSelection = true; } else { incorrectIndexSelection = false; }
        }
        while (incorrectIndexSelection);

        indexSelection -= 1;

        System.out.println("indexSelectionVal = " + indexSelection);

        try
        {
            System.out.println(personArray[indexSelection].toString());
        } catch (Exception e) {
            System.out.println("Error "+e.getMessage());
            e.printStackTrace();
        }
        return;
    }

    public static void editStaffFromList(Person personFromList, Scanner input)
    {
        boolean incorrectEntry = false;

        do {
            incorrectEntry = false;

            System.out.println("Select the corresponding number you wish to change");
            System.out.println(
                    "0) Back to main menu\n"+
                            "1) 5 digit ID number:\n" +
                            "2) First name:\n" +
                            "3) Last name:\n" +
                            "4) Birth-date as YYYY/MM/DD with slashes:\n" +
                            "5) Address (Minimum 10 characters):\n" +
                            "6) Department name:\n" +
                            "7) Supervisor name:\n" +
                            "8) start-date (yyyy/MM/dd):\n" +
                            "9) salary, must be entered as numeric amount");

            switch (input.nextLine())
            {
                case "0": return;
                case "1":
                    boolean errorIsTrue = false;
                    do {
                        errorIsTrue = false;
                        try {
                            System.out.println("Please enter your 5 digit ID number");
                            do {

                            } while (personFromList.setId(Integer.parseInt(input.nextLine())) == false);
                        } catch (Exception e) {
                            errorIsTrue = true;
                            System.out.println("Must input 5 digit ID number! ERROR " + e.getMessage().toString());
                        }
                    } while (errorIsTrue);
                    break;
                case "2":
                    System.out.println("Please enter new first name");
                    do {
                        System.out.println("Please enter between 2 and 50 letters");
                    } while (personFromList.setFirstName(input.nextLine()) == false);
                    break;
                case "3":
                    do {
                        System.out.println("Please enter the new last name");
                        System.out.println("Please enter between 2 and 50 letters");
                    } while (personFromList.setLastName(input.nextLine()) == false);
                    break;
                case "4":
                    System.out.println("Please enter the new birth-date as YYYY/MM/DD with slashes");
                    do {
                    } while (personFromList.setBirthDate(input.nextLine()) == false);
                    break;
                case "5":
                    System.out.println("Please enter your address");
                    do {
                    } while (personFromList.setAddress(input.nextLine()) == false);
                    break;
                case "6":
                    System.out.println("Enter department name!");
                    do {
                    } while (((Staff) personFromList).setDepartment(input.nextLine()) == false);
                    break;
                case "7":
                    System.out.println("Enter supervisor name!");
                    do {
                    } while (((Staff) personFromList).setSupervisor(input.nextLine()) == false);
                    break;
                case "8":
                    do
                    {
                        System.out.println("Enter your start-date (yyyy/MM/dd):");
                    }
                    //Since these classes implement the same interface the interface can be
                    //casted to any classes that implement it.
                    while (((Employee) personFromList).setStartDate(input.nextLine()) == false);
                    break;
                case "9":
                    do
                    {
                        errorIsTrue = false;
                        try {
                            do {
                                System.out.println("Enter your salary, must be entered as numeric amount!");
                            } while (((Employee) personFromList).setSalary(Double.parseDouble(input.nextLine())) == false);
                        } catch (Exception e) {
                            System.out.println("ERROR! " + e.getMessage());
                            errorIsTrue = true;
                        }
                    } while (errorIsTrue);
                    break;
                default:
                    incorrectEntry = true;
                    break;
            }
        } while (incorrectEntry);
        //allowing the user to view the updated profile
        System.out.println("---Updated Profile---");
        System.out.println(personFromList.toString());
    }

    public static void editFacultyFullTimeFromList(Person personFromList, Scanner input)
    {
        System.out.println("Select the corresponding number you wish to change");
        System.out.println(
                "0) Back to main menu\n"+
                        "1)  5 digit ID number:\n" +
                        "2)  First name:\n" +
                        "3)  Last name:\n" +
                        "4)  Birth-date as YYYY/MM/DD with slashes:\n" +
                        "5)  Address (Minimum 10 characters):\n" +
                        "6)  Division name:\n" +
                        "7)  Degree:\n" +
                        "8)  Start-date (yyyy/MM/dd):\n" +
                        "9)  Salary, must be entered as numeric amount:\n" +
                        "10) Year of graduation\n" +
                        "11) Rank");

        switch (input.nextLine())
        {
            case "0": return;
            case "1":
                boolean errorIsTrue = false;
                do {
                    errorIsTrue = false;
                    try {
                        System.out.println("Please enter your 5 digit ID number");
                        do {

                        } while (personFromList.setId(Integer.parseInt(input.nextLine())) == false);
                    } catch (Exception e) {
                        errorIsTrue = true;
                        System.out.println("Must input 5 digit ID number! ERROR " + e.getMessage().toString());
                    }
                } while (errorIsTrue);
                break;
            case "2":
                System.out.println("Please enter new first name");
                do {
                    System.out.println("Please enter between 2 and 50 letters");
                } while (personFromList.setFirstName(input.nextLine()) == false);
                break;
            case "3":
                System.out.println("Please enter the new last name");
                do {
                    System.out.println("Please enter between 2 and 50 letters");
                } while (personFromList.setLastName(input.nextLine()) == false);
                break;
            case "4":
                System.out.println("Please enter the new birth-date as YYYY/MM/DD with slashes");
                do {
                } while (personFromList.setBirthDate(input.nextLine()) == false);
                break;
            case "5":
                System.out.println("Please enter your address");
                do {
                } while (personFromList.setAddress(input.nextLine()) == false);
                break;
            case "6":
                System.out.println("Enter division name!");
                do {
                } while (((FullTime) personFromList).setDivision(input.nextLine()) == false);
                break;
            case "7":
                System.out.println("Select degree!");
                //The user will input their degree
                System.out.println("Please enter your degree");
                System.out.println("Enter" +
                        "\nB for Bachelor's"+
                        "\nM for Master's"+
                        "\nD for Doctorate");
                do {

                } while (((FullTime) personFromList).setDegree(input.nextLine()) == false);
                break;
            case "8":
                do
                {
                    try {
                        System.out.println("Enter your start-date (yyyy/MM/dd):");
                    } catch (Exception e) {
                        System.out.println("Error! " + e.getMessage());
                    }
                }
                //Since these classes implement the same interface the interface can be
                //casted to any classes that implement it.
                while (((Employee) personFromList).setStartDate(input.nextLine()) == false);
                break;
            case "9":
                do
                {
                    errorIsTrue = false;
                    try {
                        do {
                            System.out.println("Enter your salary, must be entered as numeric amount!");
                        } while (((Employee) personFromList).setSalary(Double.parseDouble(input.nextLine())) == false);
                    } catch (Exception e) {
                        System.out.println("ERROR! " + e.getMessage());
                        errorIsTrue = true;
                    }
                } while (errorIsTrue);
                break;
            case "10":
                do
                {
                    errorIsTrue = false;
                    try
                    {
                        //The user will input their graduation year
                        System.out.println("Please enter your year of graduation");
                        System.out.println("Must be between 1950 and current year!");
                        do{
                        }
                        while (((FullTime) personFromList).setYearOfGraduation(Integer.parseInt(input.nextLine())) == false);
                    }
                    catch (Exception e)
                    {
                        System.out.println("ERROR! " + e.getMessage());
                        errorIsTrue = true;
                    }
                }while (errorIsTrue);
                break;
            case "11":
                //The user will input their rank
                System.out.println("Please enter your rank");
                System.out.println( "\n1 for Lecturer" +
                        "\n2 for Assistant Professor" +
                        "\n3 for Associate Professor" +
                        "\n4 for Full Professor");
                do {
                }while (((FullTime) personFromList).setRank(input.nextLine()) == false);
                break;
            default:
                break;
        }
        //allowing the user to view the updated profile
        System.out.println("---Updated Profile---");
        System.out.println(personFromList.toString());
    }

    public static void editFacultyPartTimeFromList(Person personFromList, Scanner input)
    {
        System.out.println("Select the corresponding number you wish to change");
        System.out.println(
                "0) Back to main menu\n"+
                        "1)  5 digit ID number:\n" +
                        "2)  First name:\n" +
                        "3)  Last name:\n" +
                        "4)  Birth-date as YYYY/MM/DD with slashes:\n" +
                        "5)  Address (Minimum 10 characters):\n" +
                        "6)  Division name:\n" +
                        "7)  Degree:\n" +
                        "8)  Start-date (yyyy/MM/dd):\n" +
                        "9)  Salary, must be entered as numeric amount:\n" +
                        "10) Year of graduation:\n" +
                        "11) Number of courses taught:");

        switch (input.nextLine())
        {
            case "0": return;
            case "1":
                boolean errorIsTrue = false;
                do {
                    errorIsTrue = false;
                    try {
                        System.out.println("Please enter your 5 digit ID number");
                        do {

                        } while (personFromList.setId(Integer.parseInt(input.nextLine())) == false);
                    } catch (Exception e) {
                        errorIsTrue = true;
                        System.out.println("Must input 5 digit ID number! ERROR " + e.getMessage().toString());
                    }
                } while (errorIsTrue);
                break;
            case "2":
                System.out.println("Please enter new first name");
                do {
                    System.out.println("Please enter between 2 and 50 letters");
                } while (personFromList.setFirstName(input.nextLine()) == false);
                break;
            case "3":
                System.out.println("Please enter the new last name");
                do {
                    System.out.println("Please enter between 2 and 50 letters");
                } while (personFromList.setLastName(input.nextLine()) == false);
                break;
            case "4":
                System.out.println("Please enter the new birth-date as YYYY/MM/DD with slashes");
                do {
                } while (personFromList.setBirthDate(input.nextLine()) == false);
                break;
            case "5":
                System.out.println("Please enter your address");
                do {
                } while (personFromList.setAddress(input.nextLine()) == false);
                break;
            case "6":
                System.out.println("Enter division name!");
                do {
                } while (((PartTime) personFromList).setDivision(input.nextLine()) == false);
                break;
            case "7":
                System.out.println("Select degree!");
                //The user will input their degree
                System.out.println("Please enter your degree");
                System.out.println("Enter" +
                        "\nB for Bachelor's"+
                        "\nM for Master's"+
                        "\nD for Doctorate");
                do {

                } while (((PartTime) personFromList).setDegree(input.nextLine()) == false);
                break;
            case "8":
                do
                {
                    try {
                        System.out.println("Enter your start-date (yyyy/MM/dd):");
                    } catch (Exception e) {
                        System.out.println("Error! " + e.getMessage());
                    }
                }
                //Since these classes implement the same interface the interface can be
                //casted to any classes that implement it.
                while (((Employee) personFromList).setStartDate(input.nextLine()) == false);
                break;
            case "9":
                do
                {
                    errorIsTrue = false;
                    try {
                        do {
                            System.out.println("Enter your salary, must be entered as numeric amount!");
                        } while (((Employee) personFromList).setSalary(Double.parseDouble(input.nextLine())) == false);
                    } catch (Exception e) {
                        System.out.println("ERROR! " + e.getMessage());
                        errorIsTrue = true;
                    }
                } while (errorIsTrue);
                break;
            case "10":
                do
                {
                    errorIsTrue = false;
                    try
                    {
                        //The user will input their graduation year
                        System.out.println("Please enter your year of graduation");
                        System.out.println("Must be between 1950 and current year!");
                        do{
                        }
                        while (((PartTime) personFromList).setYearOfGraduation(Integer.parseInt(input.nextLine())) == false);
                    }
                    catch (Exception e)
                    {
                        System.out.println("ERROR! " + e.getMessage());
                        errorIsTrue = true;
                    }
                }while (errorIsTrue);
                break;
            case "11":
                do
                {
                    errorIsTrue = false;
                    try
                    {
                        //The user will input their rank
                        System.out.println("Please enter the number of courses taught(between 1 and 100)");
                        do {

                        }while (((PartTime) personFromList).setCoursesTaught(input.nextLine()) == false);
                    }
                    catch (Exception e)
                    {
                        errorIsTrue = true;
                        System.out.println("ERROR! "+e.getMessage());
                    }
                }while (errorIsTrue);
                break;
            default:
                break;
        }
        //allowing the user to view the updated profile
        System.out.println("---Updated Profile---");
        System.out.println(personFromList.toString());
    }

    public static void editStudentFromList(Person personFromList, Scanner input)
    {
        System.out.println("Select the corresponding number you wish to change");
        System.out.println(
                "0) Back to main menu\n"+
                        "1) 5 digit ID number:\n" +
                        "2) First name:\n" +
                        "3) Last name:\n" +
                        "4) Birth-date as YYYY/MM/DD with slashes:\n" +
                        "5) Address (Minimum 10 characters):\n" +
                        "6) Class Standing:\n" +
                        "7) Permanent Address:\n" +
                        "8) GPA:");

        switch (input.nextLine())
        {
            case "0": return;
            case "1":
                boolean errorIsTrue = false;
                do {
                    errorIsTrue = false;
                    try {
                        System.out.println("Please enter your 5 digit ID number");
                        do {

                        } while (personFromList.setId(Integer.parseInt(input.nextLine())) == false);
                    } catch (Exception e) {
                        errorIsTrue = true;
                        System.out.println("Must input 5 digit ID number! ERROR " + e.getMessage().toString());
                    }
                } while (errorIsTrue);
                break;
            case "2":
                System.out.println("Please enter new first name");
                do {
                    System.out.println("Please enter between 2 and 50 letters");
                } while (personFromList.setFirstName(input.nextLine()) == false);
                break;
            case "3":
                System.out.println("Please enter the new last name");
                do {
                    System.out.println("Please enter between 2 and 50 letters");
                } while (personFromList.setLastName(input.nextLine()) == false);
                break;
            case "4":
                System.out.println("Please enter the new birth-date as YYYY/MM/DD with slashes");
                do {
                } while (personFromList.setBirthDate(input.nextLine()) == false);
                break;
            case "5":
                System.out.println("Please enter your address");
                do {
                } while (personFromList.setAddress(input.nextLine()) == false);
                break;
            case "6":
                do {
                    System.out.println("Please enter class standing!");
                    System.out.println("\nF for Freshmen \nS for Sophomore \nJ for Junior \nS for Senior \nG for Graduate\n");
                } while (((Student) personFromList).setClassStanding(input.nextLine()) == false);
                break;
            case "7":
                do {
                    System.out.println("Please enter new permanent address: ");
                }while (((Student) personFromList).setPermanentAddress(input.nextLine()) == false);
                break;
            case "8":
                do {
                    errorIsTrue = false;
                    try {
                        System.out.println("Please enter new GPA!");
                        do {

                        } while (((Student) personFromList).setGpa(Double.parseDouble(input.nextLine())) == false);
                    } catch (Exception e) {
                        errorIsTrue = true;
                        System.out.println("ERROR! " + e.getMessage() +
                                "\nMust Enter A DIGIT between 0 and 4!");
                    }
                } while (errorIsTrue);
                break;
            default:
                break;
        }
        //allowing the user to view the updated profile
        System.out.println("---Updated Profile---");
        System.out.println(personFromList.toString());
    }

    public static void editTutorFromList(Person personFromList, Scanner input)
    {
        boolean incorrectEntry = false;
        do {

            incorrectEntry = false;

            System.out.println("Select the corresponding number you wish to change");
            System.out.println(
                    "0)  Back to main menu\n"+
                            "1)  5 digit ID number:\n" +
                            "2)  First name:\n" +
                            "3)  Last name:\n" +
                            "4)  Birth-date as YYYY/MM/DD with slashes:\n" +
                            "5)  Address (Minimum 10 characters):\n" +
                            "6)  Class Standing:\n" +
                            "7)  Permanent Address:\n" +
                            "8)  GPA:\n" +
                            "9)  Topics:\n" +
                            "10) Hours:\n" +
                            "11) Start-Date:\n" +
                            "12) Salary:");

            switch (input.nextLine())
            {
                case "0":
                    return;
                case "1":
                    boolean errorIsTrue = false;
                    do {
                        errorIsTrue = false;
                        try {
                            System.out.println("Please enter your 5 digit ID number");
                            do {

                            } while (personFromList.setId(Integer.parseInt(input.nextLine())) == false);
                        } catch (Exception e) {
                            errorIsTrue = true;
                            System.out.println("Must input 5 digit ID number! ERROR " + e.getMessage().toString());
                        }
                    } while (errorIsTrue);
                    break;
                case "2":
                    System.out.println("Please enter new first name");
                    do {
                        System.out.println("Please enter between 2 and 50 letters");
                    } while (personFromList.setFirstName(input.nextLine()) == false);
                    break;
                case "3":
                    System.out.println("Please enter the new last name");
                    do {
                        System.out.println("Please enter between 2 and 50 letters");
                    } while (personFromList.setLastName(input.nextLine()) == false);
                    break;
                case "4":
                    do {
                        try
                        {
                            System.out.println("Enter updated birth-date (yyyy/MM/dd):");
                        }
                        catch (Exception e){
                            System.out.println("Error! " + e.getMessage());
                        }
                    }
                    while (personFromList.setBirthDate(input.nextLine()) == false);
                    break;
                case "5":
                    System.out.println("Please enter your address");
                    do {
                    } while (personFromList.setAddress(input.nextLine()) == false);
                    break;
                case "6":
                    do {
                        System.out.println("Please enter class standing!");
                        System.out.println("\nF for Freshmen \nS for Sophomore \nJ for Junior \nS for Senior \nG for Graduate\n");
                    } while (((Tutor) personFromList).setClassStanding(input.nextLine()) == false);
                    break;
                case "7":
                    do {
                        System.out.println("Please enter new permanent address: ");
                    }while (((Tutor) personFromList).setPermanentAddress(input.nextLine()) == false);
                    break;
                case "8":
                    do {
                        errorIsTrue = false;
                        try {
                            System.out.println("Please enter new GPA!");
                            do {

                            } while (((Tutor) personFromList).setGpa(Double.parseDouble(input.nextLine())) == false);
                        } catch (Exception e) {
                            errorIsTrue = true;
                            System.out.println("ERROR! " + e.getMessage() +
                                    "\nMust Enter A DIGIT between 0 and 4!");
                        }
                    } while (errorIsTrue);
                    break;
                case "9":
                    do {
                        System.out.println("Enter updated topics tutored:");
                    }while (((Tutor) personFromList).setTopics(input.nextLine()) == false);
                    break;
                case "10":
                    do{
                        errorIsTrue = false;
                        System.out.println("Enter new number of hours tutored");
                        try {
                            do {

                            }while (((Tutor) personFromList).setHours(Double.parseDouble(input.nextLine())) == false);
                        }catch (Exception e){
                            System.out.println("ERROR! "+e.getMessage());
                            errorIsTrue = true;
                        }
                    } while (errorIsTrue);
                    break;
                case "11":
                    do {
                        try
                        {
                            System.out.println("Enter updated start-date (yyyy/MM/dd):");
                        }
                        catch (Exception e){
                            System.out.println("Error! " + e.getMessage());
                        }
                    }
                    while (((Tutor) personFromList).setStartDate(input.nextLine()) == false);
                    break;
                case "12":
                    do{
                        errorIsTrue = false;
                        try
                        {
                            do
                            {
                                System.out.println("Enter the updated salary, must be entered as numeric amount!");
                            }while (((Tutor) personFromList).setSalary(Double.parseDouble(input.nextLine())) == false);
                        }
                        catch (Exception e)
                        {
                            System.out.println("ERROR! "+e.getMessage());
                            errorIsTrue = true;
                        }
                    } while (errorIsTrue);
                    break;
                default:
                    incorrectEntry = true;
                    break;
            }
        } while (incorrectEntry);
        //allowing the user to view the updated profile
        System.out.println("---Updated Profile---");
        System.out.println(personFromList.toString());
    }

    public static void editStaff(Person[]personArray, int indexSelection, Scanner input)
    {
        boolean incorrectEntry = false;

        do {
            incorrectEntry = false;

            System.out.println("Select the corresponding number you wish to change");
            System.out.println(
                    "0) Back to main menu\n"+
                            "1) 5 digit ID number:\n" +
                            "2) First name:\n" +
                            "3) Last name:\n" +
                            "4) Birth-date as YYYY/MM/DD with slashes:\n" +
                            "5) Address (Minimum 10 characters):\n" +
                            "6) Department name:\n" +
                            "7) Supervisor name:\n" +
                            "8) start-date (yyyy/MM/dd):\n" +
                            "9) salary, must be entered as numeric amount");

            switch (input.nextLine())
            {
                case "0": return;
                case "1":
                    boolean errorIsTrue = false;
                    do {
                        errorIsTrue = false;
                        try {
                            System.out.println("Please enter your 5 digit ID number");
                            do {

                            } while (personArray[indexSelection].setId(Integer.parseInt(input.nextLine())) == false);
                        } catch (Exception e) {
                            errorIsTrue = true;
                            System.out.println("Must input 5 digit ID number! ERROR " + e.getMessage().toString());
                        }
                    } while (errorIsTrue);
                    break;
                case "2":
                    System.out.println("Please enter new first name");
                    do {
                        System.out.println("Please enter between 2 and 50 letters");
                    } while (personArray[indexSelection].setFirstName(input.nextLine()) == false);
                    break;
                case "3":
                    do {
                        System.out.println("Please enter the new last name");
                        System.out.println("Please enter between 2 and 50 letters");
                    } while (personArray[indexSelection].setLastName(input.nextLine()) == false);
                    break;
                case "4":
                    System.out.println("Please enter the new birth-date as YYYY/MM/DD with slashes");
                    do {
                    } while (personArray[indexSelection].setBirthDate(input.nextLine()) == false);
                    break;
                case "5":
                    System.out.println("Please enter your address");
                    do {
                    } while (personArray[indexSelection].setAddress(input.nextLine()) == false);
                    break;
                case "6":
                    System.out.println("Enter department name!");
                    do {
                    } while (((Staff) personArray[indexSelection]).setDepartment(input.nextLine()) == false);
                    break;
                case "7":
                    System.out.println("Enter supervisor name!");
                    do {
                    } while (((Staff) personArray[indexSelection]).setSupervisor(input.nextLine()) == false);
                    break;
                case "8":
                    do
                    {
                        System.out.println("Enter your start-date (yyyy/MM/dd):");
                    }
                    //Since these classes implement the same interface the interface can be
                    //casted to any classes that implement it.
                    while (((Employee) personArray[indexSelection]).setStartDate(input.nextLine()) == false);
                    break;
                case "9":
                    do
                    {
                        errorIsTrue = false;
                        try {
                            do {
                                System.out.println("Enter your salary, must be entered as numeric amount!");
                            } while (((Employee) personArray[indexSelection]).setSalary(Double.parseDouble(input.nextLine())) == false);
                        } catch (Exception e) {
                            System.out.println("ERROR! " + e.getMessage());
                            errorIsTrue = true;
                        }
                    } while (errorIsTrue);
                    break;
                default:
                    incorrectEntry = true;
                    break;
            }
        } while (incorrectEntry);
        //allowing the user to view the updated profile
        System.out.println("---Updated Profile---");
        System.out.println(personArray[indexSelection].toString());
    }


    public static void editFacultyFullTime(Person[]personArray, int indexSelection, Scanner input)
    {
        System.out.println("Select the corresponding number you wish to change");
        System.out.println(
                "0) Back to main menu\n"+
                        "1)  5 digit ID number:\n" +
                        "2)  First name:\n" +
                        "3)  Last name:\n" +
                        "4)  Birth-date as YYYY/MM/DD with slashes:\n" +
                        "5)  Address (Minimum 10 characters):\n" +
                        "6)  Division name:\n" +
                        "7)  Degree:\n" +
                        "8)  Start-date (yyyy/MM/dd):\n" +
                        "9)  Salary, must be entered as numeric amount:\n" +
                        "10) Year of graduation\n" +
                        "11) Rank");

        switch (input.nextLine())
        {
            case "0": return;
            case "1":
                boolean errorIsTrue = false;
                do {
                    errorIsTrue = false;
                    try {
                        System.out.println("Please enter your 5 digit ID number");
                        do {

                        } while (personArray[indexSelection].setId(Integer.parseInt(input.nextLine())) == false);
                    } catch (Exception e) {
                        errorIsTrue = true;
                        System.out.println("Must input 5 digit ID number! ERROR " + e.getMessage().toString());
                    }
                } while (errorIsTrue);
                break;
            case "2":
                System.out.println("Please enter new first name");
                do {
                    System.out.println("Please enter between 2 and 50 letters");
                } while (personArray[indexSelection].setFirstName(input.nextLine()) == false);
                break;
            case "3":
                System.out.println("Please enter the new last name");
                do {
                    System.out.println("Please enter between 2 and 50 letters");
                } while (personArray[indexSelection].setLastName(input.nextLine()) == false);
                break;
            case "4":
                System.out.println("Please enter the new birth-date as YYYY/MM/DD with slashes");
                do {
                } while (personArray[indexSelection].setBirthDate(input.nextLine()) == false);
                break;
            case "5":
                System.out.println("Please enter your address");
                do {
                } while (personArray[indexSelection].setAddress(input.nextLine()) == false);
                break;
            case "6":
                System.out.println("Enter division name!");
                do {
                } while (((FullTime) personArray[indexSelection]).setDivision(input.nextLine()) == false);
                break;
            case "7":
                System.out.println("Select degree!");
                //The user will input their degree
                System.out.println("Please enter your degree");
                System.out.println("Enter" +
                        "\nB for Bachelor's"+
                        "\nM for Master's"+
                        "\nD for Doctorate");
                do {

                } while (((FullTime) personArray[indexSelection]).setDegree(input.nextLine()) == false);
                break;
            case "8":
                do
                {
                    try {
                        System.out.println("Enter your start-date (yyyy/MM/dd):");
                    } catch (Exception e) {
                        System.out.println("Error! " + e.getMessage());
                    }
                }
                //Since these classes implement the same interface the interface can be
                //casted to any classes that implement it.
                while (((Employee) personArray[indexSelection]).setStartDate(input.nextLine()) == false);
                break;
            case "9":
                do
                {
                    errorIsTrue = false;
                    try {
                        do {
                            System.out.println("Enter your salary, must be entered as numeric amount!");
                        } while (((Employee) personArray[indexSelection]).setSalary(Double.parseDouble(input.nextLine())) == false);
                    } catch (Exception e) {
                        System.out.println("ERROR! " + e.getMessage());
                        errorIsTrue = true;
                    }
                } while (errorIsTrue);
                break;
            case "10":
                do
                {
                    errorIsTrue = false;
                    try
                    {
                        //The user will input their graduation year
                        System.out.println("Please enter your year of graduation");
                        System.out.println("Must be between 1950 and current year!");
                        do{
                        }
                        while (((FullTime) personArray[indexSelection]).setYearOfGraduation(Integer.parseInt(input.nextLine())) == false);
                    }
                    catch (Exception e)
                    {
                        System.out.println("ERROR! " + e.getMessage());
                        errorIsTrue = true;
                    }
                }while (errorIsTrue);
                break;
            case "11":
                //The user will input their rank
                System.out.println("Please enter your rank");
                System.out.println( "\n1 for Lecturer" +
                        "\n2 for Assistant Professor" +
                        "\n3 for Associate Professor" +
                        "\n4 for Full Professor");
                do {
                }while (((FullTime) personArray[indexSelection]).setRank(input.nextLine()) == false);
                break;
            default:
                break;
        }
        //allowing the user to view the updated profile
        System.out.println("---Updated Profile---");
        System.out.println(personArray[indexSelection].toString());
    }

    public static void editFacultyPartTime(Person[]personArray, int indexSelection, Scanner input)
    {
        System.out.println("Select the corresponding number you wish to change");
        System.out.println(
                "0) Back to main menu\n"+
                        "1)  5 digit ID number:\n" +
                        "2)  First name:\n" +
                        "3)  Last name:\n" +
                        "4)  Birth-date as YYYY/MM/DD with slashes:\n" +
                        "5)  Address (Minimum 10 characters):\n" +
                        "6)  Division name:\n" +
                        "7)  Degree:\n" +
                        "8)  Start-date (yyyy/MM/dd):\n" +
                        "9)  Salary, must be entered as numeric amount:\n" +
                        "10) Year of graduation:\n" +
                        "11) Number of courses taught:");

        switch (input.nextLine())
        {
            case "0": return;
            case "1":
                boolean errorIsTrue = false;
                do {
                    errorIsTrue = false;
                    try {
                        System.out.println("Please enter your 5 digit ID number");
                        do {

                        } while (personArray[indexSelection].setId(Integer.parseInt(input.nextLine())) == false);
                    } catch (Exception e) {
                        errorIsTrue = true;
                        System.out.println("Must input 5 digit ID number! ERROR " + e.getMessage().toString());
                    }
                } while (errorIsTrue);
                break;
            case "2":
                System.out.println("Please enter new first name");
                do {
                    System.out.println("Please enter between 2 and 50 letters");
                } while (personArray[indexSelection].setFirstName(input.nextLine()) == false);
                break;
            case "3":
                System.out.println("Please enter the new last name");
                do {
                    System.out.println("Please enter between 2 and 50 letters");
                } while (personArray[indexSelection].setLastName(input.nextLine()) == false);
                break;
            case "4":
                System.out.println("Please enter the new birth-date as YYYY/MM/DD with slashes");
                do {
                } while (personArray[indexSelection].setBirthDate(input.nextLine()) == false);
                break;
            case "5":
                System.out.println("Please enter your address");
                do {
                } while (personArray[indexSelection].setAddress(input.nextLine()) == false);
                break;
            case "6":
                System.out.println("Enter division name!");
                do {
                } while (((PartTime) personArray[indexSelection]).setDivision(input.nextLine()) == false);
                break;
            case "7":
                System.out.println("Select degree!");
                //The user will input their degree
                System.out.println("Please enter your degree");
                System.out.println("Enter" +
                        "\nB for Bachelor's"+
                        "\nM for Master's"+
                        "\nD for Doctorate");
                do {

                } while (((PartTime) personArray[indexSelection]).setDegree(input.nextLine()) == false);
                break;
            case "8":
                do
                {
                    try {
                        System.out.println("Enter your start-date (yyyy/MM/dd):");
                    } catch (Exception e) {
                        System.out.println("Error! " + e.getMessage());
                    }
                }
                //Since these classes implement the same interface the interface can be
                //casted to any classes that implement it.
                while (((Employee) personArray[indexSelection]).setStartDate(input.nextLine()) == false);
                break;
            case "9":
                do
                {
                    errorIsTrue = false;
                    try {
                        do {
                            System.out.println("Enter your salary, must be entered as numeric amount!");
                        } while (((Employee) personArray[indexSelection]).setSalary(Double.parseDouble(input.nextLine())) == false);
                    } catch (Exception e) {
                        System.out.println("ERROR! " + e.getMessage());
                        errorIsTrue = true;
                    }
                } while (errorIsTrue);
                break;
            case "10":
                do
                {
                    errorIsTrue = false;
                    try
                    {
                        //The user will input their graduation year
                        System.out.println("Please enter your year of graduation");
                        System.out.println("Must be between 1950 and current year!");
                        do{
                        }
                        while (((PartTime) personArray[indexSelection]).setYearOfGraduation(Integer.parseInt(input.nextLine())) == false);
                    }
                    catch (Exception e)
                    {
                        System.out.println("ERROR! " + e.getMessage());
                        errorIsTrue = true;
                    }
                }while (errorIsTrue);
                break;
            case "11":
                do
                {
                    errorIsTrue = false;
                    try
                    {
                        //The user will input their rank
                        System.out.println("Please enter the number of courses taught(between 1 and 100)");
                        do {

                        }while (((PartTime) personArray[indexSelection]).setCoursesTaught(input.nextLine()) == false);
                    }
                    catch (Exception e)
                    {
                        errorIsTrue = true;
                        System.out.println("ERROR! "+e.getMessage());
                    }
                }while (errorIsTrue);
                break;
            default:
                break;
        }
        //allowing the user to view the updated profile
        System.out.println("---Updated Profile---");
        System.out.println(personArray[indexSelection].toString());
    }

    public static void editStudent(Person[]personArray, int indexSelection, Scanner input)
    {
        System.out.println("Select the corresponding number you wish to change");
        System.out.println(
                "0) Back to main menu\n"+
                        "1) 5 digit ID number:\n" +
                        "2) First name:\n" +
                        "3) Last name:\n" +
                        "4) Birth-date as YYYY/MM/DD with slashes:\n" +
                        "5) Address (Minimum 10 characters):\n" +
                        "6) Class Standing:\n" +
                        "7) Permanent Address:\n" +
                        "8) GPA:");

        switch (input.nextLine())
        {
            case "0": return;
            case "1":
                boolean errorIsTrue = false;
                do {
                    errorIsTrue = false;
                    try {
                        System.out.println("Please enter your 5 digit ID number");
                        do {

                        } while (personArray[indexSelection].setId(Integer.parseInt(input.nextLine())) == false);
                    } catch (Exception e) {
                        errorIsTrue = true;
                        System.out.println("Must input 5 digit ID number! ERROR " + e.getMessage().toString());
                    }
                } while (errorIsTrue);
                break;
            case "2":
                System.out.println("Please enter new first name");
                do {
                    System.out.println("Please enter between 2 and 50 letters");
                } while (personArray[indexSelection].setFirstName(input.nextLine()) == false);
                break;
            case "3":
                System.out.println("Please enter the new last name");
                do {
                    System.out.println("Please enter between 2 and 50 letters");
                } while (personArray[indexSelection].setLastName(input.nextLine()) == false);
                break;
            case "4":
                System.out.println("Please enter the new birth-date as YYYY/MM/DD with slashes");
                do {
                } while (personArray[indexSelection].setBirthDate(input.nextLine()) == false);
                break;
            case "5":
                System.out.println("Please enter your address");
                do {
                } while (personArray[indexSelection].setAddress(input.nextLine()) == false);
                break;
            case "6":
                do {
                    System.out.println("Please enter class standing!");
                    System.out.println("\nF for Freshmen \nS for Sophomore \nJ for Junior \nS for Senior \nG for Graduate\n");
                } while (((Student) personArray[indexSelection]).setClassStanding(input.nextLine()) == false);
                break;
            case "7":
                do {
                    System.out.println("Please enter new permanent address: ");
                }while (((Student) personArray[indexSelection]).setPermanentAddress(input.nextLine()) == false);
                break;
            case "8":
                do {
                    errorIsTrue = false;
                    try {
                        System.out.println("Please enter new GPA!");
                        do {

                        } while (((Student) personArray[indexSelection]).setGpa(Double.parseDouble(input.nextLine())) == false);
                    } catch (Exception e) {
                        errorIsTrue = true;
                        System.out.println("ERROR! " + e.getMessage() +
                                "\nMust Enter A DIGIT between 0 and 4!");
                    }
                } while (errorIsTrue);
                break;
            default:
                break;
        }
        //allowing the user to view the updated profile
        System.out.println("---Updated Profile---");
        System.out.println(personArray[indexSelection].toString());
    }

    public static void editTutor(Person[]personArray, int indexSelection, Scanner input)
    {
        boolean incorrectEntry = false;
        do {

            incorrectEntry = false;

            System.out.println("Select the corresponding number you wish to change");
            System.out.println(
                    "0)  Back to main menu\n"+
                            "1)  5 digit ID number:\n" +
                            "2)  First name:\n" +
                            "3)  Last name:\n" +
                            "4)  Birth-date as YYYY/MM/DD with slashes:\n" +
                            "5)  Address (Minimum 10 characters):\n" +
                            "6)  Class Standing:\n" +
                            "7)  Permanent Address:\n" +
                            "8)  GPA:\n" +
                            "9)  Topics:\n" +
                            "10) Hours:\n" +
                            "11) Start-Date:\n" +
                            "12) Salary:");

            switch (input.nextLine())
            {
                case "0":
                    return;
                case "1":
                    boolean errorIsTrue = false;
                    do {
                        errorIsTrue = false;
                        try {
                            System.out.println("Please enter your 5 digit ID number");
                            do {

                            } while (personArray[indexSelection].setId(Integer.parseInt(input.nextLine())) == false);
                        } catch (Exception e) {
                            errorIsTrue = true;
                            System.out.println("Must input 5 digit ID number! ERROR " + e.getMessage().toString());
                        }
                    } while (errorIsTrue);
                    break;
                case "2":
                    System.out.println("Please enter new first name");
                    do {
                        System.out.println("Please enter between 2 and 50 letters");
                    } while (personArray[indexSelection].setFirstName(input.nextLine()) == false);
                    break;
                case "3":
                    System.out.println("Please enter the new last name");
                    do {
                        System.out.println("Please enter between 2 and 50 letters");
                    } while (personArray[indexSelection].setLastName(input.nextLine()) == false);
                    break;
                case "4":
                    do {
                        try
                        {
                            System.out.println("Enter updated birth-date (yyyy/MM/dd):");
                        }
                        catch (Exception e){
                            System.out.println("Error! " + e.getMessage());
                        }
                    }
                    while (personArray[indexSelection].setBirthDate(input.nextLine()) == false);
                    break;
                case "5":
                    System.out.println("Please enter your address");
                    do {
                    } while (personArray[indexSelection].setAddress(input.nextLine()) == false);
                    break;
                case "6":
                    do {
                        System.out.println("Please enter class standing!");
                        System.out.println("\nF for Freshmen \nS for Sophomore \nJ for Junior \nS for Senior \nG for Graduate\n");
                    } while (((Tutor) personArray[indexSelection]).setClassStanding(input.nextLine()) == false);
                    break;
                case "7":
                    do {
                        System.out.println("Please enter new permanent address: ");
                    }while (((Tutor) personArray[indexSelection]).setPermanentAddress(input.nextLine()) == false);
                    break;
                case "8":
                    do {
                        errorIsTrue = false;
                        try {
                            System.out.println("Please enter new GPA!");
                            do {

                            } while (((Tutor) personArray[indexSelection]).setGpa(Double.parseDouble(input.nextLine())) == false);
                        } catch (Exception e) {
                            errorIsTrue = true;
                            System.out.println("ERROR! " + e.getMessage() +
                                    "\nMust Enter A DIGIT between 0 and 4!");
                        }
                    } while (errorIsTrue);
                    break;
                case "9":
                    do {
                        System.out.println("Enter updated topics tutored:");
                    }while (((Tutor) personArray[indexSelection]).setTopics(input.nextLine()) == false);
                    break;
                case "10":
                    do{
                        errorIsTrue = false;
                        System.out.println("Enter new number of hours tutored");
                        try {
                            do {

                            }while (((Tutor) personArray[indexSelection]).setHours(Double.parseDouble(input.nextLine())) == false);
                        }catch (Exception e){
                            System.out.println("ERROR! "+e.getMessage());
                            errorIsTrue = true;
                        }
                    } while (errorIsTrue);
                    break;
                case "11":
                    do {
                        try
                        {
                            System.out.println("Enter updated start-date (yyyy/MM/dd):");
                        }
                        catch (Exception e){
                            System.out.println("Error! " + e.getMessage());
                        }
                    }
                    while (((Tutor) personArray[indexSelection]).setStartDate(input.nextLine()) == false);
                    break;
                case "12":
                    do{
                        errorIsTrue = false;
                        try
                        {
                            do
                            {
                                System.out.println("Enter the updated salary, must be entered as numeric amount!");
                            }while (((Tutor) personArray[indexSelection]).setSalary(Double.parseDouble(input.nextLine())) == false);
                        }
                        catch (Exception e)
                        {
                            System.out.println("ERROR! "+e.getMessage());
                            errorIsTrue = true;
                        }
                    } while (errorIsTrue);
                    break;
                default:
                    incorrectEntry = true;
                    break;
            }
        } while (incorrectEntry);
        //allowing the user to view the updated profile
        System.out.println("---Updated Profile---");
        System.out.println(personArray[indexSelection].toString());
    }

    /**
     * These methods are strictly for testing, they quickly fill the array with different combinations
     * of objects of faculty, staff, and student
     */
    public static Person[] CreateArrayOfStaff(Person[] objectArray, int numOfObjs)
    {
        Person[] tempArray = new Person[objectArray.length];

        for (int i = 0; i < numOfObjs; i++)
        {
            tempArray[i] = new Staff();
            tempArray[i].setFirstName(ExtractClassName(tempArray[i].getClass().toString()));
            System.out.println("Creating test " + tempArray[i].getClass().toString() );
        }

        return tempArray;
    }

    public static Person[] CreateArrayOfFullTime(Person[] objectArray, int numOfObjs)
    {
        Person[] tempArray = new Person[objectArray.length];

        for (int i = 0; i < numOfObjs; i++)
        {
            tempArray[i] = new FullTime();
            tempArray[i].setFirstName(ExtractClassName(tempArray[i].getClass().toString()));
            System.out.println("Creating test " + tempArray[i].getClass().toString() );
        }

        return tempArray;
    }

    public static Person[] CreateArrayOfPartTime(Person[] objectArray, int numOfObjs)
    {
        Person[] tempArray = new Person[objectArray.length];

        for (int i = 0; i < numOfObjs; i++)
        {
            tempArray[i] = new PartTime();
            tempArray[i].setFirstName(ExtractClassName(tempArray[i].getClass().toString()));
            System.out.println("Creating test " + tempArray[i].getClass().toString() );
        }

        return tempArray;
    }

    public static Person[] CreateArrayOfStudent(Person[] objectArray, int numOfObjs)
    {
        Person[] tempArray = new Person[objectArray.length];

        for (int i = 0; i < numOfObjs; i++)
        {
            tempArray[i] = new Student();
            tempArray[i].setFirstName(ExtractClassName(tempArray[i].getClass().toString()));
            System.out.println("Creating test " + tempArray[i].getClass().toString() );
        }

        return tempArray;
    }

    public static Person[] CreateArrayOfTutor(Person[] objectArray, int numOfObjs)
    {
        Person[] tempArray = new Person[objectArray.length];

        for (int i = 0; i < numOfObjs; i++)
        {
            tempArray[i] = new Tutor();
            tempArray[i].setFirstName(ExtractClassName(tempArray[i].getClass().toString()));
            System.out.println("Creating test " + tempArray[i].getClass().toString() );
        }

        return tempArray;
    }

    public static Person[] CreateArrayOfEachObj(Person[] objectArray, int numOfObjs)
    {
        Person[] tempArray = new Person[objectArray.length];

        for (int i = 0; i < numOfObjs; i++)
        {
            if(i == 0)
            {
                tempArray[i] = new Staff();
                tempArray[i].setFirstName(ExtractClassName(tempArray[i].getClass().toString()));
                System.out.println("Creating test " + tempArray[i].getClass().toString() );
            }
            if(i == 1)
            {
                tempArray[i] = new PartTime();
                tempArray[i].setFirstName(ExtractClassName(tempArray[i].getClass().toString()));
                System.out.println("Creating test " + tempArray[i].getClass().toString() );
            }
            if(i == 2)
            {
                tempArray[i] = new FullTime();
                tempArray[i].setFirstName(ExtractClassName(tempArray[i].getClass().toString()));
                System.out.println("Creating test " + tempArray[i].getClass().toString() );
            }
            if(i == 3)
            {
                tempArray[i] = new Student();
                tempArray[i].setFirstName(ExtractClassName(tempArray[i].getClass().toString()));
                System.out.println("Creating test " + tempArray[i].getClass().toString() );
            }
            if(i == 4)
            {
                tempArray[i] = new Tutor();
                tempArray[i].setFirstName(ExtractClassName(tempArray[i].getClass().toString()));
                System.out.println("Creating test " + tempArray[i].getClass().toString() );
            }
        }
        return tempArray;
    }

    public static String ExtractClassName(String getClassName)
    {
        String tempStringToReturn = "";
        String tempStringToReverse = "";

        for (int i = (getClassName.length() - 1); i >= 0 ; i--)
        {
            if (getClassName.charAt(i) != '.')
            {
                tempStringToReverse = tempStringToReverse + getClassName.charAt(i);
                //System.out.println(tempStringToReverse);
            }else { break; }
        }
        for (int i = (tempStringToReverse.length() - 1); i >= 0; i--)
        {
            tempStringToReturn = tempStringToReturn + tempStringToReverse.charAt(i);
            //System.out.println(tempStringToReturn);
        }

        return tempStringToReturn;
    }
}
